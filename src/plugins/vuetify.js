// For test use. Do not include createVuetify()
// see https://next.vuetifyjs.com/en/features/treeshaking/
// import * as components from "vuetify/components";
// import * as directives from "vuetify/directives";

import { createVuetify } from "vuetify";
import { md2 } from "vuetify/blueprints";
import { aliases, mdi } from "vuetify/iconsets/mdi-svg";

import icons from "./icons";

import "vuetify/styles";
// Translations provided by Vuetify
import { en, de, nl } from "vuetify/locale";

const lsmLightTheme = {
  dark: false,
  colors: {
    background: "#ffdde3",
    surface: "#FFFFFF",
    surface_invert: "#000000",
    "surface-variant": "#696969",
    primary: "#ff002b",
    //primary: "#FF3B42",
    "primary-darken-1": "#ed0025",
    "primary-darken-2": "#e0001c",
    "primary-darken-3": "#d3000b",
    "primary-lighten-1": "#ff1b29",
    "primary-lighten-2": "#ff3e49",
    secondary: "#00ffd5",
    "secondary-darken-1": "#00f9c2",
    third: "#000000",
    error: "#ff002b",
    info: "#00ffd5",
    success: "#33ffc9",
    warning: "#ff6333",
    green: "#00ff00",
  },
};

const lsmDarkTheme = {
  dark: true,
  colors: {
    background: "#05181e",
    surface: "#000000",
    surface_invert: "#FFFFFF",
    primary: "#FF3B42",
    "primary-darken-1": "#ff1920",
    "primary-darken-2": "#f60022",
    "primary-darken-3": "#d70014",
    "primary-lighten-1": "#f46669",
    "primary-lighten-2": "#fb9393",
    secondary: "#ff3ba4",
    "secondary-darken-1": "#ff008c",
    third: "#ffffff",
    error: "#B00020",
    info: "#2196F3",
    success: "#FF3B42",
    warning: "#FB8C00",
  },
};

const vuetify = createVuetify({
  // locale: {
  // TODO: i18n vuetify
  // adapter: createVueI18nAdapter({ i18n, useI18n })
  // },
  blueprint: md2,
  components: {
    //...labs,
  },
  defaults: {
    global: {
      rounded: "lg",
    },
    VAppBar: {
      flat: true,
    },
    VCard: {
      variant: "",
    },
    VBtn: {
      // set v-btn default color to primary
      color: "primary",
      //variant: "outlined",
      elevation: 0,
    },
    VAutocomplete: {
      variant: "outlined",
      density: "comfortable",
      color: "primary",
      hideDetails: "auto",
    },
    VTextField: {
      density: "comfortable",
      color: "primary",
      hideDetails: "auto",
    },
    VDataTable: {
      variant: "outlined",
      density: "comfortable",
      color: "primary",
      hideDetails: "auto",
    },
  },
  icons: {
    defaultSet: "mdi",
    aliases: {
      ...aliases,
      ...icons,
    },
    sets: {
      mdi,
    },
  },
  // Internationalization (i18n)
  // https://next.vuetifyjs.com/en/features/internationalization/#internationalization-i18n
  locale: {
    locale: "de",
    fallback: "de",
    messages: { de, en, nl },
  },

  // Theme
  // https://next.vuetifyjs.com/en/features/theme/
  theme: {
    defaultTheme: "lsmLightTheme",
    themes: {
      lsmLightTheme,
      lsmDarkTheme,
    },
  },
});

export default vuetify;
