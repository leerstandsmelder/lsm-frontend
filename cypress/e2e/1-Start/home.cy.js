/// <reference types="cypress" />

describe("Home screen", () => {
  beforeEach(() => {
    cy.intercept_all_apis();

    cy.log('Cypress.env("baseUrl")....', Cypress.env("baseUrl"));
    cy.visit("/");
  });

  it("displays layout elements", () => {
    cy.get("main.v-main").should("have.length", 1);
    cy.get(".v-footer").should("have.length", 2);
  });
  it("loads and displays news", () => {
    cy.get(".news-list").should("contain", "Neuigkeiten");
    cy.get(".news-list", { timeout: 10000 })
      .should("contain", "Leben statt Leerstand")
      .and("contain", "auch in Hamburgeria");
  });

  it("loads and displays regions in the toolbar", () => {
    cy.viewport("macbook-16");

    cy.get("#regionBtn").click();

    cy.get(".region-list").should("have.length", 1);

    cy.get(".region-list").children().should("have.length", 3); // 2 entries + subheader
  });
});
