// Pinia Store
import { defineStore, acceptHMRUpdate } from "pinia";
import { RepositoryFactory } from "@/utils/repositories/repositoryFactory";
const PlaceRepository = RepositoryFactory.get("place");
const ImagesRepository = RepositoryFactory.get("images");
const CommentsRepository = RepositoryFactory.get("comments");
import { usePlaceStore } from "@/stores/place";

export const useCommentStore = defineStore({
  id: "comment",
  state: () => ({
    status: "initial",
    comment: {
      title: "",
      text: "",
      status: "",
      images: [],
    },
  }),
  getters: {
    getComment: (state) => state.comment,
    images: (state) => state.comment.images,
  },
  actions: {
    async fetchComment(commentId) {
      return await CommentsRepository.get(commentId).then((data) => {
        this.comment = data;
        const placeStore = usePlaceStore();
        if (this.comment.place_id != placeStore.place.id) {
          placeStore.setReloadPlace({ place: { id: this.comment.place_id } });
        }
      });
    },

    async deleteComment(payload) {
      return await CommentsRepository.destroy(payload.slug, payload.commentId);
      // this.clear();
    },
    upload(payload) {
      const placeStore = usePlaceStore();
      let placeID = this.comment.place_id || placeStore.place.id;
      const { file, event } = payload;
      const params = {
        title: "",
        source: "lsm-frontend",
        place_id: placeID,
        file: file,
        imageable_type: "Annotation",
        imageable_id: this.comment.id,
      };

      return ImagesRepository.upload(params, event).then((new_image) => {
        if (!this.comment.images) this.comment.images = [];
        this.comment.images.push(new_image);

        return new_image;
      });
    },
    fetchImages() {
      const ImagesRepository = RepositoryFactory.get("images");
      return ImagesRepository.get(this.comment.id, "Comment").then((data) => {
        this.comment.images = data;
        return data;
      });
    },
    createComment() {
      return CommentsRepository.create(this.comment).then((data) => {
        console.log("create comment store", data);
        this.comment = data;
        return data;
      });
    },
    updateComment() {
      return CommentsRepository.update(this.comment).then((data) => {
        console.log("update comment store", data);
        this.comment = data;
        return data;
      });
    },
    clear() {
      this.$reset();
    },
  },
  persist: false,
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useCommentStore, import.meta.hot));
}
