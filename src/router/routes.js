const MyPlaces = () => import("../views/MyPlaces.vue");
const Chart = () => import("../views/Chart.vue");
const Map = () => import("../views/Map.vue");
const Form = () => import("../views/Form.vue");
const Comment = () => import("../views/Comment.vue");
const Create = () => import("../views/Create.vue");
const Search = () => import("../views/Search.vue");
const News = () => import("../views/News.vue");
const Imprint = () => import("../views/Imprint.vue");
const Register = () => import("../views/Users/Register.vue");
//const ForgotPassword = () => import("../views/Users/ForgotPassword.vue");
const ForgotPassword = () => import("../views/Users/ForgotPassword.vue");

const AdminDashboard = () => import("../views/Admin/Dashboard.vue");
const AdminRegions = () => import("../views/Admin/Regions.vue");
const AdminUsers = () => import("../views/Admin/Users.vue");
const AdminPlaces = () => import("../views/Admin/Places.vue");
const StyleGuide = () => import("../views/Styleguide.vue");
const NotFound = () => import("../views/NotFound.vue");

const routes = [
  {
    path: "/",
    name: "home",
    title: "site.subnav.home",
    icon: "$mdiHome",
    component: () => import("../views/Home.vue"),
    meta: {
      title: "site.subnav.home",
      requiresAuth: false,
      always: true,
    },
  },
  {
    path: "/:region",
    name: "region-direct",
    icon: "$mdiHome",
    component: () => import("../views/Region.vue"),
    meta: {
      title: "site.subnav.region",
      requiresAuth: false,
    },
  },
  {
    path: "/:region/:slug",
    name: "region-place-direct",
    icon: "$mdiHome",
    component: () => import("../views/Region.vue"),
    meta: {
      title: "site.subnav.region",
      requiresAuth: false,
    },
  },

  {
    path: "/region",
    name: "region-without",
    icon: "$mdiHome",
    title: "menu.map",
    component: () => import("../views/Region.vue"),
    meta: {
      title: "site.subnav.region",
      always: true,
      requiresAuth: false,
    },
  },
  {
    path: "/region/:region",
    name: "region",
    icon: "$mdiHome",
    component: () => import("../views/Region.vue"),
    meta: {
      title: "site.subnav.region",
      requiresAuth: false,
    },
  },
  {
    path: "/region/:region/:slug",
    name: "region-place",
    icon: "$mdiHome",
    component: () => import("../views/Region.vue"),
    meta: {
      title: "site.subnav.region",
      requiresAuth: false,
    },
  },
  {
    path: "/details/:slug",
    name: "place-detail",
    icon: "$mdiPencil",
    component: () => import("../views/Place.vue"),
    meta: {
      title: "menu.detail",
      requiresAuth: false,
    },
  },

  {
    path: "/region/:region/details/:slug",
    name: "region-place-detail",
    icon: "$mdiPencil",
    component: () => import("../views/Place.vue"),
    meta: {
      title: "menu.detail",
      requiresAuth: false,
    },
  },

  // {
  //   path: "/div",
  //   name: "divider-admin",
  //   title: "site.subnav.information",
  //   meta: {
  //     requiresAuth: false,
  //   },
  // },
  // {
  //   path: "/map",
  //   name: "map",
  //   icon: "$mdiMap",
  //   title: "menu.map",
  //   component: Map,
  //   meta: {
  //     title: "menu.map",
  //     always: true,
  //   },
  // },
  // {
  //   name: "map-show",
  //   path: "/map/:slug",
  //   component: Map,
  //   props: true,
  //   meta: {
  //     requiresAuth: false,
  //   },
  // },

  {
    path: "/form",
    name: "form",
    icon: "$mdiPencil",
    component: Form,
    meta: {
      title: "menu.form",
      requiresAuth: true,
      always: false,
    },
  },

  {
    path: "/region/:region/form/:slug",
    name: "region-place-form",
    icon: "$mdiPencil",
    component: Form,
    meta: {
      title: "menu.form",
      requiresAuth: false,
      always: false,
    },
  },
  {
    path: "/:region/:slug/edit",
    name: "region-place-edit",
    icon: "$mdiPencil",
    component: Form,
    meta: {
      title: "menu.form",
      requiresAuth: false,
      always: false,
    },
  },

  {
    path: "/create",
    name: "create",
    icon: "$mdiPencil",
    title: "menu.form",
    component: Create,
    meta: {
      title: "menu.form",
      requiresAuth: true,
      always: true,
    },
  },
  {
    path: "/search",
    name: "search",
    icon: "$mdiMagnify",
    component: Search,
    meta: {
      title: "menu.search",
      always: false,
    },
  },
  {
    path: "/imprint",
    name: "imprint",
    icon: "$mdiMap",
    component: Imprint,
    meta: {
      title: "menu.imprint",
      requiresAuth: false,
      always: true,
    },
  },
  {
    path: "/form/:slug",
    name: "form-detail",
    icon: "$mdiPencil",
    component: Form,
    meta: {
      title: "menu.form_edit",
      requiresAuth: true,
    },
  },
  {
    path: "/comment/:slug",
    name: "comment-form",
    icon: "$mdiPencil",
    component: Comment,
    meta: {
      title: "menu.comment",
      requiresAuth: true,
      always: false,
    },
  },
  {
    path: "/chart",
    name: "chart",
    icon: "$mdiChartLine",
    title: "menu.statistic",
    component: Chart,
    meta: {
      title: "menu.statistic",
      requiresAuth: true,
    },
  },

  {
    path: "/login",
    name: "login",
    icon: "$mdiLogin",
    title: "users.login",
    component: () => import("../views/Users/Login.vue"),
    meta: {
      title: "users.login",
      requiresAuth: false,
    },
  },
  {
    path: "/register",
    name: "register",
    icon: "$mdiAccountPlus",
    title: "users.register",
    component: () => import("../views/Users/Register.vue"),
    meta: {
      title: "users.register",
      requiresAuth: false,
    },
  },
  {
    path: "/confirm/:token/:email",
    name: "confirmation",
    component: () => import("../views/Users/Register.vue"),
    meta: {
      title: "users.confirmation",
      requiresAuth: false,
    },
  },
  {
    path: "/forgot-password",
    name: "forgot-password",
    icon: "$mdiLogin",
    component: () => import("../views/Users/ForgotPassword.vue"),
    meta: {
      title: "users.forgot",
      requiresAuth: false,
    },
  },
  {
    path: "/reset-password",
    name: "reset-password",
    icon: "$mdiLogin",
    component: () => import("../views/Users/ResetPassword.vue"),
    meta: {
      title: "users.reset",
      requiresAuth: false,
    },
  },
  {
    path: "/profile",
    name: "profile",
    icon: "$mdiAccount",
    //title: "menu.profil",
    component: () => import("../views/Users/Profile.vue"),
    meta: {
      title: "menu.profile",
      requiresAuth: true,
    },
  },
  {
    path: "/my-locations",
    name: "my-locations",
    icon: "$mdiMarker",
    //title: "menu.my-locations",
    component: MyPlaces,
    meta: {
      title: "menu.my-locations",
      requiresAuth: true,
    },
  },

  {
    path: "/div2",
    name: "divider-admin",
    title: "menu.admin",
    meta: {
      title: "menu.admin",
      requiresAuth: true,
      requiresRole: "admin",
    },
  },
  {
    path: "/admin",
    name: "admin_dashboard",
    icon: "$mdiChartLine",
    title: "menu.admin_menu.dashboard",
    component: AdminDashboard,
    meta: {
      requiresAuth: true,
      requiresRole: "admin",
    },
    children: [],
  },
  {
    path: "/admin/regions",
    name: "admin_region",
    icon: "$mdiChartLine",
    title: "menu.admin_menu.regions",

    component: AdminRegions,
    meta: {
      title: "menu.admin_menu.regions",
      requiresAuth: true,
      requiresRole: "admin",
    },
  },
  {
    path: "/admin/users",
    name: "admin_users",
    icon: "$mdiChartLine",
    title: "menu.admin_menu.users",

    component: AdminUsers,
    meta: {
      title: "menu.admin_menu.users",
      requiresAuth: true,
      requiresRole: "admin",
    },
  },
  {
    path: "/admin/locations",
    name: "admin_locations",
    icon: "$mdiChartLine",
    title: "menu.admin_menu.locations",

    component: AdminPlaces,
    meta: {
      title: "menu.admin_menu.locations",
      requiresAuth: true,
      requiresRole: "admin",
    },
  },
  {
    path: "/admin/styleguide",
    name: "Styleguide",
    icon: "$mdiNewspaper",
    title: "menu.styleguide",
    component: StyleGuide,
    meta: {
      title: "menu.styleguide",
      requiresAuth: true,
      requiresRole: "admin",
    },
  },

  // {
  //   path: "/https://www.leerstandsmelder.de",
  //   href: true,
  //   name: "leerstandsmelder",
  //   title: "site.name",
  //   icon: "open-in-new",
  //   meta: {
  //     title: "site.name",
  //     requiresAuth: false,
  //   },
  // },

  {
    path: "/div2",
    name: "divider-admin",
    title: "menu.content",
    meta: {
      title: "menu.content",
      content: true,
    },
  },
  {
    path: "/news",
    name: "news",
    icon: "$mdiNewspaper",
    title: "menu.news",
    component: News,
    meta: {
      title: "menu.news",
      content: true,
    },
  },
  {
    path: "/article/:slug",
    name: "article",
    icon: "$mdiNewspaper",
    component: News,
    meta: {
      title: "menu.article",
      content: true,
    },
  },
  {
    path: "/:pathMatch(.*)*",
    name: "NotFound",
    component: NotFound,
    meta: {
      title: "site.subnav.region",
      requiresAuth: false,
    },
  },

  // {
  //   name: "site-content",
  //   path: "/site/:slug",
  //   component: SiteContent,
  //   props: true,
  //   meta: {
  //     requiresAuth: false,
  //   },
  // },
];

export default routes;
