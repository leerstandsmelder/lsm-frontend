// Pinia Store
import { defineStore, acceptHMRUpdate } from "pinia";
import i18n from "@/i18n";
import useDetectColorScheme from "@/composables/useDetectColorScheme";

export const useSiteStore = defineStore({
  id: "site",
  // convert to a function
  state: () => ({
    language: "de",
    themeColor: "",
    siteName: "site.name",
  }),
  getters: {
    getThemeColor: (state) => {
      if (state.themeColor == "") {
        let color = useDetectColorScheme();
        return color.value;
      } else {
        return state.themeColor;
      }
    },
  },
  actions: {
    setLanguage(code) {
      i18n.global.locale.value = code;
      i18n.locale = code;
      this.language = code;
      document.querySelector("html").setAttribute("lang", code);
      this.setName();
    },
    setTheme(theme) {
      this.themeColor = theme;
    },
    setName() {
      let name = "site.name";
      if (/((^|\.)leerstandsmelder\.in)$/.test(location.hostname)) {
        name = "site.name_neutral";
      }
      this.siteName = name;
    },
  },
  persist: true,
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useSiteStore, import.meta.hot));
}
