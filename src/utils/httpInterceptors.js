import httpClient from "./api";
import { useSiteStore } from "@/stores/site";
import { useLoaderStore } from "@/stores/loader";

export function setupInterceptors() {
  let requestsPending = 0;

  const req = {
    pending: () => {
      requestsPending++;
      // const loaderStore = useLoaderStore();
      // loaderStore.show();
    },
    done: () => {
      requestsPending--;
      if (requestsPending <= 0) {
        // const loaderStore = useLoaderStore();
        // loaderStore.hide();
      }
    },
  };

  httpClient.interceptors.request.use(
    (config) => {
      req.pending();
      const siteStore = useSiteStore();
      config.params = {
        ...config.params,
        locale: siteStore.language,
      };
      //TODO: use store instead of localstorage
      config.headers.client = window.localStorage.getItem("client");
      config.headers["access-token"] =
        window.localStorage.getItem("access-token");
      config.headers.uid = window.localStorage.getItem("uid");
      config.headers["token-type"] = window.localStorage.getItem("token-type");
      config.headers["access-control-allow-origin"] = "*";
      config.headers["Access-Control-Allow-Headers"] = "Authorization";

      return config;
    },
    (error) => {
      requestsPending--;

      req.done();

      return Promise.reject(error);
    }
  );

  httpClient.interceptors.response.use(
    ({ data, headers }) => {
      req.done();
      if (headers.client) {
        localStorage.setItem("access-token", headers["access-token"]);
        localStorage.setItem("client", headers.client);
        localStorage.setItem("uid", headers.uid);
        localStorage.setItem("token-type", headers["token-type"]);
      }

      return Promise.resolve(data);
    },
    (error) => {
      req.done();

      return Promise.reject(error);
    }
  );
}
