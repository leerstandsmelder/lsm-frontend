import "cypress-localstorage-commands";

Cypress.Commands.add("intercept_all_apis", () => {
  cy.intercept("**/api/v1/maps/**", { fixture: "maps.json" }).as("getMaps"); // and assign an alias
  cy.intercept("**/api/v1/news/**", { fixture: "news.json" }).as("getNews"); // and assign an alias
  cy.intercept("**/api/v1/places.geojson/**", {
    fixture: "places_latest.geojson",
  }).as("getLatestPlaces"); // and assign an alias

  cy.intercept("**/api.maptiler.com/**", "success").as("getMaptilerApi"); // and assign an alias
});

Cypress.Commands.add("intercept_user_apis", () => {
  cy.intercept("**/me**", { fixture: "me_user.json" }).as("getMeUser"); // me

  cy.intercept("**/sign_in**", { fixture: "login_user.json" }).as("login"); // login

  cy.intercept("**/api/v1/users/**/places**", { fixture: "my_places.json" }).as(
    "my_places"
  ); // login
  cy.intercept("**/api/v1/users/**/annotations**", {
    fixture: "my_comments.json",
  }).as("my_comments"); // login

  cy.intercept("DELETE", "**/sign_out**", { fixture: "login_user.json" }).as(
    "logout"
  ); // logout
});

Cypress.Commands.add("login", (login, password) => {
  cy.intercept("**/sign_in**", {
    fixture: "login_user.json",
  }).as("login"); // login

  cy.visit("/login", { failOnStatusCode: false });
  cy.get("[name=login]").type(login);
  cy.get("[name=password]").type(password);
  cy.get("button[type=submit").click();

  cy.setLocalStorage("access-token", "xx-access-token-xx");
  cy.setLocalStorage("client", login);
  cy.setLocalStorage("uid", "xxxx");
  cy.setLocalStorage("token-type", "headers[token-type]");
  //cy.url().should('contain', '/')
});
