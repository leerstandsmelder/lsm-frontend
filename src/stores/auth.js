// Pinia Store
import { defineStore, acceptHMRUpdate } from "pinia";
import api from "@/utils/api";
import { useUserStore } from "@/stores/user";

const baseDomain = import.meta.env.VITE_API_URL;

export const useAuthStore = defineStore({
  id: "auth",
  state: () => ({
    status: "",
    token: localStorage.getItem("access-token") || "",
    email: localStorage.getItem("uid") || "",
    client: localStorage.getItem("client") || "",
    userId: localStorage.getItem("userId") || "",
    scope: localStorage.getItem("scope") || "",
  }),
  getters: {
    isLoggedIn: (state) => !!state.token,
    isAdmin: (state) => state.scope.includes("admin"),
    authStatus: (state) => state.status,
    getUser: (state) => state.userId,
    getToken: (state) => state.token,
    getRole: (state) => state.scope,
  },
  actions: {
    login(data) {
      let user = {
        email: data.email,
        password: data.password,
      };
      let url = `${baseDomain}/api/v1/auth/sign_in`;

      this.status = "loading";
      return api({
        url: url,
        data: user,
        method: "POST",
      }).then((resp) => {
        const token = localStorage.getItem("access-token");
        const email = localStorage.getItem("uid");
        if (email) {
          this.token = token;
          this.email = email;
          this.status = "success";

          return this.getCredentials().then((response) => {
            return resp;
          });
        } else {
          return "No Login";
        }
      });
    },
    getCredentials() {
      let userStore = useUserStore();
      return userStore
        .fetchUser()
        .then((resp) => {
          let attributes = resp.data.attributes || resp.data || resp;

          if (attributes) {
            this.scope = attributes.role_keys;
            this.userId = attributes.id;
            localStorage.setItem("scope", JSON.stringify(this.scope));
            localStorage.setItem("userId", this.userId);
            this.status = "credentials_success";
            return resp;
          } else {
            return "NO KEYS";
          }
        })
        .catch((err) => {
          this.status = "error";
          localStorage.removeItem("access-token");
          localStorage.removeItem("userId");
          localStorage.removeItem("scope");
          return err;
        });
    },
    init() {
      return new Promise((resolve, reject) => {
        if (localStorage.getItem("access-token")) {
          this.status = "success";
          this.token = localStorage.getItem("access-token");
          this.email = localStorage.getItem("uid");

          if (localStorage.getItem("userId") && localStorage.getItem("scope")) {
            this.scope = localStorage.getItem("scope");
            this.userId = localStorage.getItem("userId");
            this.status = "credentials_success";
            const userStore = useUserStore();
            userStore.fetchUser();
          } else {
            this.getCredentials();
            //userStore.fetchUser();
          }

          resolve("TOKEN SET");
        } else {
          //TODO: ?redirect to login?
          resolve("NO TOKEN");
        }
      });
    },
    register(user) {
      this.status = "loading";
      return api({
        url: `${baseDomain}/api/v1/auth`,
        data: user,
        method: "POST",
      }).then((resp) => {
        this.userId = resp.id;
        this.status = "registering";
        return resp;
      });
    },
    logout() {
      const userStore = useUserStore();
      return api({
        url: `${baseDomain}/api/v1/auth/sign_out`,
        method: "DELETE",
      })
        .then((resp) => {
          console.log("logout then", resp);
          localStorage.removeItem("access-token");
          localStorage.removeItem("email");
          localStorage.removeItem("client");
          localStorage.removeItem("uid");
          localStorage.removeItem("scope");
          localStorage.removeItem("userId");
          localStorage.removeItem("token-type");
          localStorage.removeItem("lsm");
          this.$reset();
          userStore.clear();
          return "LOGGED_OUT";
        })
        .catch((err) => {
          console.log("logout catch", err);
          localStorage.removeItem("access-token");
          localStorage.removeItem("email");
          localStorage.removeItem("client");
          localStorage.removeItem("uid");
          localStorage.removeItem("scope");
          localStorage.removeItem("userId");
          localStorage.removeItem("token-type");
          localStorage.removeItem("lsm");
          this.$reset();
          userStore.clear();
          return "LOGGED_OUT_ERROR";
        });
    },
    clear() {
      this.$reset();
    },
  },
  persist: true,
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useAuthStore, import.meta.hot));
}
