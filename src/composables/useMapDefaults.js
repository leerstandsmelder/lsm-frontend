import { computed } from "vue";
import { useDisplay } from "vuetify";
import i18n from "@/i18n";

export const mapGestures = computed(() => {
  return {
    "CooperativeGesturesHandler.WindowsHelpText": i18n.global.t(
      "map.WindowsHelpText"
    ),
    "CooperativeGesturesHandler.MacHelpText": i18n.global.t("map.MacHelpText"),
    "CooperativeGesturesHandler.MobileHelpText":
      i18n.global.t("map.MobileHelpText"),
  };
});

export const mapHeight = computed(() => {
  const { name } = useDisplay();
  // name is reactive and
  // must use .value
  switch (name.value) {
    case "xs":
    case "sm":
      return "600px";
    case "md":
      return "80vh";
    case "lg":
      return "70vh";
    case "xl":
      return "70vh";
    case "xxl":
      return "70vh";
  }

  return undefined;
});

export const positionOptions = { enableHighAccuracy: true };

export const clusterCountLayout = {
  "text-field": "{point_count_abbreviated}",
  "text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Bold"],
  "text-size": 12,
};
export const unclusteredPaint = {
  "circle-color": [
    "match",
    ["get", "status"],
    "alive",
    "#00FF00",
    "damage",
    "#423137",
    "temporary",
    "#e69c09",
    "unknown",
    "#ba75d1",
    "vacancy",
    "#FF0000",
    "#FF0000",
  ],

  "circle-radius": {
    base: 1.55,
    stops: [
      [8, 8],
      [18, 10],
    ],
  },
  "circle-opacity": 0.6,
};

export const unclusteredPaint2 = {
  "circle-color": [
    "match",
    ["get", "status"],
    "alive",
    "#00FF00",
    "damage",
    "#423137",
    "temporary",
    "#e69c09",
    "unknown",
    "#ba75d1",
    "vacancy",
    "#FF0000",
    "#FF0000",
  ],

  "circle-radius": {
    base: 2,
    stops: [
      [2, 4],
      [6, 4],
      [10, 6],
      [12, 10],
    ],
  },
  "circle-opacity": 0.6,
};

export const clusterCountPaint = {
  "circle-color": [
    "step",
    ["get", "point_count"],
    "#f9bae4",
    5,
    "#f98bd2",
    15,
    "#fb54bd",
  ],
  "circle-radius": ["step", ["get", "point_count"], 20, 5, 30, 15, 40],
  "circle-opacity": 0.6,
};

export const pointsPaint = {
  "circle-radius": 8,
  "circle-color": [
    "match",
    ["get", "status"],
    "alive",
    "#00FF00",
    "damage",
    "#423137",
    "temporary",
    "#e69c09",
    "unknown",
    "#ba75d1",
    "vacancy",
    "#FF0000",
    "#FF0000",
  ],
  "circle-opacity": 0.5,
};

export const searchResultPaint = {
  "circle-radius": 12,
  "circle-color": "#B42222",
  "circle-opacity": 0.5,
};
export const placeResultPaint = {
  "circle-radius": 12,
  "circle-color": "#000000",
  "circle-opacity": 0.9,
};

export function placeSelectFly(item, map) {
  //mapInstance.map.fire("flystart");

  map.flyTo({
    center: item.geometry.coordinates,
    zoom: map.getZoom(),
    bearing: 0,
    speed: 0.5, // make the flying slow
    curve: 1, // change the speed at which it zooms out
    easing: function (t) {
      return t;
    },
    essential: false,
  });
  //setPlaceClick(item, item.properties);
}

export function createDot(map, size) {
  return {
    width: size,
    height: size,
    data: new Uint8Array(size * size * 4),

    // get rendering context for the map canvas when layer is added to the map
    onAdd: function () {
      var canvas = document.createElement("canvas");
      canvas.width = this.width;
      canvas.height = this.height;
      this.context = canvas.getContext("2d");
    },

    // called once before every frame where the icon will be used
    render: function () {
      var duration = 1000;
      var t = (performance.now() % duration) / duration;

      var radius = (size / 2) * 0.3;
      var outerRadius = (size / 2) * 0.7 * t + radius;
      var context = this.context;

      // draw outer circle
      context.clearRect(0, 0, this.width, this.height);
      context.beginPath();
      context.arc(this.width / 2, this.height / 2, outerRadius, 0, Math.PI * 2);
      context.fillStyle = "rgba(255, 200, 200," + (1 - t) + ")";
      context.fill();

      // draw inner circle
      context.beginPath();
      context.arc(this.width / 2, this.height / 2, radius, 0, Math.PI * 2);
      //context.fillStyle = 'rgba(255, 100, 100, 1)';
      context.fillStyle = "transparent";
      context.strokeStyle = "white";
      context.lineWidth = 2 + 4 * (1 - t);
      context.fill();
      context.stroke();

      // update this image's data with data from the canvas
      this.data = context.getImageData(0, 0, this.width, this.height).data;

      // continuously repaint the map, resulting in the smooth animation of the dot
      map.triggerRepaint();

      // return `true` to let the map know that the image was updated
      return true;
    },
  };
}

export const geocoderApi = {
  forwardGeocode: async (config) => {
    const features = [];
    try {
      const request = `https://nominatim.openstreetmap.org/search?q=${config.query}&format=geojson&polygon_geojson=1&addressdetails=1`;
      const response = await fetch(request);
      const geojson = await response.json();
      for (const feature of geojson.features) {
        const center = [
          feature.bbox[0] + (feature.bbox[2] - feature.bbox[0]) / 2,
          feature.bbox[1] + (feature.bbox[3] - feature.bbox[1]) / 2,
        ];
        const point = {
          type: "Feature",
          geometry: {
            type: "Point",
            coordinates: center,
          },
          place_name: feature.properties.display_name,
          properties: feature.properties,
          text: feature.properties.display_name,
          place_type: ["place"],
          center,
        };
        features.push(point);
      }
    } catch (e) {
      console.error(`Failed to forwardGeocode with error: ${e}`);
    }

    return {
      features,
    };
  },
};
