/// <reference types="cypress" />

describe("News screen", () => {
  beforeEach(() => {
    cy.intercept_all_apis();

    cy.log('Cypress.env("baseUrl")....', Cypress.env("baseUrl"));
    cy.visit("/news");
  });

  it("loads and displays news", () => {
    cy.get(".news-cards")
      .should("contain", "Leben statt Leerstand")
      .and("contain", "auch in Hamburgeria");
  });

  it("click on news changes url and details", () => {
    cy.get(".news-card-title").eq(2).should("contain.text", "Hamburgeria");
    cy.get(".news-card-title").eq(2).click();
    cy.location("href").should("contain", "hamburg");
    cy.get(".news-detail h2").should("contain.text", "Hamburgeria");
  });
});
