describe("User login/logout screens", () => {
  beforeEach(() => {
    cy.intercept_all_apis();

    cy.intercept_user_apis();
  });

  it("logged in as user, visits profile and logs out", () => {
    cy.login("user@domain.org", "123456789");
    cy.log("login user");

    cy.visit("/profile", { failOnStatusCode: false });
    cy.get("input[name=email]").should("have.value", "user@domain.org");
    cy.get("#menu-account").click();
    cy.get("#menu-logout").click();
    cy.url().should("include", "/");
  });
});
