import api from "@/utils/api.js";

export default {
  search(filter, page) {
    let filter_query = [];
    if (filter) {
      filter_query.push("q=" + filter);
    }
    if (page) {
      filter_query.push("page=" + page);
    }
    let filter_str = filter_query.length ? "?" + filter_query.join("&") : "";
    let url = "/search/" + filter_str;
    return api.get(url);
  },
};
