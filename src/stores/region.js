// Pinia Store
import { defineStore, acceptHMRUpdate } from "pinia";
import { RepositoryFactory } from "@/utils/repositories/repositoryFactory";

import router from "@/router";
import { useLocationsStore } from "@/stores/locations";
import { usePlaceStore } from "@/stores/place";

export const useRegionStore = defineStore({
  id: "region",
  state: () => ({
    status: "initial",
    regionId: "",
    regionData: "",
    regionMap: {
      selectedLocation: "",
      lat: 10,
      lon: 52,
      zoom: "",
      bounds: [
        [0, 0],
        [0, 0],
      ],
    },
  }),
  getters: {
    regionStatus: (state) => state.status,
    regionUrl: (state) =>
      state.regionData.slug || state.regionData.title.toLowerCase(),
    getregionData: (state) => state.regionData,
    getregionMap: (state) => state.regionMap,
    mapcenter: (state) => [
      state.regionData.mapcenter_lon,
      state.regionData.mapcenter_lat,
    ],
    mapzoom: (state) => state.regionData.zoom,
    getregionId: (state) => state.regionId,
    getregion: (state) => state.region,
    mapDefaultStatus: (state) => {
      console.log("GETTER mapDefaultStatus (regionData)", state.regionData);
      if (state.regionData && state.regionData.status) {
        let default_element = state.regionData.status.find(
          (x) => x.icon == "vacancy"
        );
        return default_element.id || "";
      }
      return "";
    },
    map_status_entries: (state) => {
      if (state.regionData && state.regionData.status) {
        let collection = [];
        collection = state.regionData.status.map((x) => ({
          title: x.title,
          value: x.id,
        }));
        return collection;
      }
      return [{ title: "Leerstand", value: "vacancy" }];
    },
  },
  actions: {
    load() {
      if (this.status == "initial" || this.status == undefined) {
        this.fetchRegion();
      }
    },

    fetchRegion(id) {
      const MapsRepository = RepositoryFactory.get("maps");
      if (id != "" && id != undefined) {
        this.status = "loading";
        return MapsRepository.getMap(id).then((resp) => {
          let map = resp.map;
          this.regionId = map.id;
          this.regionData = map;
          this.regionMap.lat = map.mapcenter_lat;
          this.regionMap.lon = map.mapcenter_lon;
          this.regionMap.zoom = map.zoom || 16;

          let new_route = "region";

          if (
            [
              "chart",
              "map",
              "region-place",
              "region-place-direct",
              "region-direct",
              "form",
              "region-place-form",
              "region-place-edit",
              "create",
            ].includes(router.currentRoute.value.name) &&
            router.currentRoute.value.params.region ==
              (map.slug || map.title.toLowerCase())
          ) {
            new_route = router.currentRoute.value.name;
          }
          router
            .push({
              name: new_route,
              params: { region: map.slug || map.title.toLowerCase() },
              replace: true,
            })
            .then((result) => {
              // console.log("router result", result);
            })
            .catch((err) => {
              console.log("router catch", err);
            });

          this.status = "loaded";

          const locationsStore = useLocationsStore();
          locationsStore.fetchLocations();
          const placeStore = usePlaceStore();
          placeStore.clear();
          return map;
        });
      }
    },

    setMap(data) {
      return new Promise((resolve, reject) => {
        this.regionMap.lat = data.lat || "";
        this.regionMap.lon = data.lon || "";
        this.regionMap.zoom = data.zoom || 16;
        resolve(data);
      });
    },
    setLocalZoom(zoom) {
      this.regionMap.zoom = zoom;
    },

    clear() {
      return new Promise((resolve, reject) => {
        this.$reset();
        const locationsStore = useLocationsStore();
        locationsStore.clear();
        router
          .push({
            name: "home",
            replace: true,
          })
          .then((result) => {
            resolve("cleared");
          })
          .catch((err) => {
            console.log("router catch", err);
          });
      });
    },
  },
  persist: true,
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useRegionStore, import.meta.hot));
}
