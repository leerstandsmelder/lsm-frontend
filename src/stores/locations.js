// Pinia Store
import { defineStore, acceptHMRUpdate } from "pinia";
import { RepositoryFactory } from "@/utils/repositories/repositoryFactory";
import grouper from "@/utils/grouper.js";
import moment from "moment";
import { useRegionStore } from "@/stores/region";

export const useLocationsStore = defineStore({
  id: "locations",
  state: () => ({
    status: "initial",
    locations: [],
    startDate: "2010-01",
    endDate: moment(new Date()).format("YYYY-MM"),
    dateField: "created_at",
    filterField: "building_type",
    filterValue: [""],
    // field => value list
    filters: {
      //owner: ["city"],
    },
    latestLocations: [],
  }),
  getters: {
    getStartDate: (state) => state.startDate,
    getEndDate: (state) => state.endDate,
    getDateField: (state) => state.dateField,
    getFilterField: (state) => state.filterField,
    locationsStatus: (state) => state.status,
    getLocations: (state) => state.locations,
    getLatestLocations: (state) => state.latestLocations.features,
    updateableLocations: (state) => {
      let sortedLocs = [];
      console.log('updateable1', state.locations);
      if (state.locations && state.locations.features) {
        let copyLocations = state.filteredLocations;
        console.log('updateable2', copyLocations);
        sortedLocs = filterDates(
          copyLocations,
          state.startDate,
          moment(state.endDate).subtract(2, "years"),
          state.dateField
        );
      }
      console.log('updateable3', sortedLocs);
      return sortedLocs.sort((a, b) => {
        return (
          new Date(a.properties[state.dateField]) -
          new Date(b.properties[state.dateField])
        );
      });
    },
    filteredLocations: (state) => {
      let calculatedLocs = [];
      if (state.locations && state.locations.features) {
        let copyLocations = [...state.locations.features];
        let datedLocs = filterDates(
          copyLocations,
          state.startDate,
          state.endDate,
          state.dateField
        );
        if (Object.keys(state.filters).length) {
          for (const [filter_field, filter_values] of Object.entries(
            state.filters
          )) {
            calculatedLocs = datedLocs.filter((item) => {
              if (item.properties[filter_field]) {
                if (Array.isArray(item.properties[filter_field])) {
                  return item.properties[filter_field].some((r) =>
                    filter_values.includes(r)
                  );
                } else {
                  let found = filter_values.includes(
                    item.properties[filter_field]
                  );
                  return found;
                }
              } else {
                return true;
              }
            });
          }
        } else {
          calculatedLocs = datedLocs;
        }
      }
      return calculatedLocs;
    },
    datedLocations: (state) => {
      let datedLocs = [];
      if (state.locations && state.locations.features) {
        let copyLocations = [...state.locations.features];
        datedLocs = filterDates(
          copyLocations,
          state.startDate,
          state.endDate,
          state.dateField
        );
      }
      return datedLocs;
    },
  },
  actions: {
    load() {
      if (this.status == "initial" || this.status == undefined) {
        this.fetchLocations();
      }
    },
    fetchLatestLocations(data) {
      const PlaceRepository = RepositoryFactory.get("place");
      return PlaceRepository.getGeoJson("", { limit: 10, include_region: true })
        .then((geojson) => {
          this.latestLocations = geojson;
          return geojson;
        })
        .catch((err) => {
          this.latestLocations = [];
          return err;
        });
    },
    fetchLocations(data) {
      const regionStore = useRegionStore();
      if (
        regionStore.regionData != "" &&
        regionStore.regionData.id != undefined
      ) {
        const MapsRepository = RepositoryFactory.get("maps");
        return MapsRepository.getLayerGeoJson(
          regionStore.regionData.id,
          regionStore.regionData.default_layer
        )
          .then((geojson) => {
            this.status = "loaded";
            this.startDate = moment(regionStore.regionData.created_at).format(
              "YYYY-MM"
            );
            this.locations = geojson;
            return geojson;
          })
          .catch((err) => {
            this.status = "error";
            return err;
          });
      } else {
        const PlaceRepository = RepositoryFactory.get("place");
        return PlaceRepository.getGeoJson()
          .then((geojson) => {
            this.status = "loaded";
            this.locations = geojson;

            return geojson;
          })
          .catch((err) => {
            this.status = "error";
            return err;
          });
      }
    },
    setDateFilter(payload) {
      this.startDate = payload.startDate;
      this.endDate = payload.endDate;
      this.dateField = payload.dateField;
    },
    setDateField(dateField) {
      this.dateField = dateField;
    },
    setStartDate(dateValue) {
      this.startDate = dateValue;
    },
    setEndDate(dateValue) {
      this.endDate = dateValue;
    },
    addFilter(filter) {
      this.filters[filter.key] = filter.value;
    },
    clear() {
      this.$reset();
    },
  },
  persist: false,
});

function filterDates(locs, start, end, field) {
  let datedLocs = [];
  if (start != "" && end != "" && field != "") {
    datedLocs = grouper.groupInBetween(locs, start, end, field);
  } else {
    console.log("dates not set");
    datedLocs = [];
  }
  return datedLocs;
}

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useLocationsStore, import.meta.hot));
}
