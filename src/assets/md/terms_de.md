## Nutzungsbedingungen

Der Leerstandsmelder soll so frei nutzbar sein wie möglich. Allerdings müssen leider auch wir uns vor allerlei rechtlichen Dingen schützen und vor allem den Missbrauch von Leerstandsmelder rechtlich ausschließen. Daher diese umfassenden Nutzungsbedingungen. Bitte lesen Sie die folgenden Nutzungsbedingungen durch. Durch Nutzung dieser Website erklären Sie Ihr Einverständnis mit diesen Nutzungsbedingungen.

**Stand: August 2023**

1. Die nachfolgenden Nutzungsbedingungen des Verein Gängeviertel e.V. (nachfolgend „Gängeviertel e.V.“) gelten für die Webseiten (nachfolgend "Website")
   www.leerstandsmelder.de
   www.leerstandsmelder.in
   www.leerstandsmeldung.de
   www.leegstandsmelder.org
   Für die Nutzung der Website ist wichtig, dass Sie als Leser:innen und/oder Verfasser:innen von Inhalten auf der Website (nachfolgend „Nutzer:innen“) die nachfolgenden Bestimmungen beachten.
2. Durch die Nutzung unserer Website sind Sie mit den Nutzungsbedingungen unserer Website einverstanden. Sie versichern uns, dass Sie keine Beiträge erstellen werden, die gegen unsere Nutzungsbedingungen verstoßen.
3. Gegenstand des Leerstandsmelder ist, Nutzer:innen zu ermöglichen, kostenlos leerstehende Gebäude auf einer Karte zu verorten und zu veröffentlichen. Mit diesen Leerstandsmeldungen soll die Leerstandssituation in verschiedenen Städten und Regionen dokumentiert und statistisch ausgewertet und es soll ein Diskurs über Leerstand und Nutzungsmöglichkeiten angeregt werden. Der Abruf der veröffentlichten Informationen erfolgt über das Internet, sowohl über stationäre wie auch über mobile Endgeräte.

### § 1 – Inhalt

(1) Gängeviertel e.V. stellt den Nutzer:innen auf der Website ein Portal zur Verfügung, auf dem sie Leerstände in Gebäuden melden und eine Beschreibung der Gebäude veröffentlichen können (nachfolgend: „Leerstandsmeldung“). Die Einträge können von allen eingesehen werden.

(2) Eine Leerstandsmeldung besteht aus allgemeinen Angaben zum Objekt (ggf. Name, Adresse, Nutzungsart, Bild, etc.) (nachfolgend „Basisdaten“).

(3) Die Nutzung der Website ist kostenlos.

### § 2 – Nutzung

(1) Die Nutzung der Website erfordert eine vorherige Registrierung soweit eigene Inhalte eingestellt werden sollen.

(2) Zur Registrierung ist ein Profilname und ein Passwort zu wählen. Unzulässig sind Profilnamen, deren Nutzung Rechte Dritter, insbesondere Markenrechte oder Namensrechte verletzt. Unzulässig sind außerdem rechtswidrige oder gegen die guten Sitten verstoßende Profilnamen. Der Profilname ist für alle anderen Nutzer:innen sichtbar. Alle weiteren Informationen und personenbezogenen Daten sind nur dann öffentlich sichtbar, wenn sie durch den/die Nutzer:in dafür freigegeben sind oder der/die Nutzer:in freiwillige, als öffentlich gekennzeichnete Angaben macht.

(3) Für eine Leerstandsmeldung wird dem/der Nutzer:in ein Eingabeformular für eine einheitliche Formatierung der Leerstandsmeldungen bereitgestellt, Inhalt und Umfang des einzelnen Berichts liegen im Ermessen der Nutzer:innen.

(4) Die Basisdaten für eine Leerstandsmeldung werden durch eigene Angaben des Nutzers bzw. der Nutzerin festgelegt.

(5) Die Basisdaten der Leerstandsmeldungen sind für alle Nutzer:innen öffentlich sichtbar.

(6) Die Administratoren haben die Möglichkeit, die Basisdaten zu redigieren, verändern oder korrigieren. Von dieser Funktion werden sie Gebrauch machen, um Informationen zu vervollständigen oder zu korrigieren oder um Leerstandsmeldungen zusammenzuführen, wenn sie doppelt im System sind. Derartige Veränderungen werden als solche gekennzeichnet.

### § 3 – Rechte und Pflichten der Nutzer:innen

(1) Die Website darf ausschließlich zu rechtlich erlaubten Zwecken genutzt werden. Jede Nutzerin bzw. jeder Nutzer kann sich kostenlos auf der Website informieren und sich auch durch Leerstandsmeldungen oder durch das Einstellen/Hochladen von Gebäudefotos oder sonstiger Inhalte und Informationen beteiligen.

(2) Es ist zulässig, die bestehenden Inhalte für private, interne Zwecke herunterzuladen. Eine gewerbliche Weitergabe der Inhalte an Dritte bedarf der vorherigen Zustimmung des Gängeviertel e.V.

(3) Die Website darf nicht dazu genutzt werden, um diffamierendes, pornografisches oder in sonstiger Weise rechtswidriges Material zu verbreiten, Dritte zu bedrohen, zu belästigen oder die Rechte (einschließlich der Persönlichkeitsrechte) Dritter zu verletzen.

(4) Soweit der/die Nutzer:in Daten eingibt, die Namen, Adressen und Informationen von Dritten enthalten, die ggf. nicht öffentlich zugänglich sind und bei denen ein berechtigtes Interesse dieses Dritten an der Geheimhaltung seiner persönlichen Daten bestehen könnte, hat der/die Nutzer:in vor einer Veröffentlichung auf der Website von dem Dritten eine Erlaubnis zur Veröffentlichung auf der Website und der Verwendung auf den durch diese Bedingungen definierten Nutzungswegen einzuholen. Im übrigen wird ausdrücklich auf die weiteren Bestimmungen in § 3, 4 und 5 verwiesen.

(5) Es ist insbesondere verboten, Bilder, Beiträge oder sonstige Inhalte der folgenden Art auf der Website zu veröffentlichen:
Inhalte, welche Rechte Dritter verletzen können, insbesondere deren Urheberrecht, Leistungsschutzrecht, Namens- und Markenrecht, Geschmacksmusterrecht, Eigentumsrecht und Persönlichkeitsrecht; pornografische, obszöne, sexistische, diffamierende, verleumderische, rassistische, Minderheiten oder religiös verletzende Darstellungen; Inhalte, welche die Rechtsordnung verletzen, Diskriminierung, Diffamierung oder Beleidigung; unwahre Behauptungen; Schmähkritik, Rufschädigung, Spekulationen oder Verleumdungen; Herabwürdigung des Urheber oder der abgebildeten Person/en; Politische Beiträge bzw. Werbung für Parteien, Boykottaufrufe oder Löschungsaufforderungen jeglicher Art, Links auf Internetseiten mit rechtswidrigen Inhalten.

(6) Bei der Angabe von Daten zu einem Ort verpflichtet sich der/die Nutzer:in außerdem:
Hinsichtlich der Fakten wie Name und Lage möglichst genau zu sein, keine Inhalte einzustellen, die vorsätzlich oder fahrlässig unwahr sind, keine falschen oder unsachlichen Bewertungen abzugeben, positive als auch negative Beiträge im Rahmen der Meinungsfreiheit nur zu dem Zweck abzugeben, anderen Nutzer:innen eine Möglichkeit zu geben, sich ein aussagekräftigeres/objektiveres/umfassenderes Bild bezüglich eines Ortes und damit zusammenhängender Gegebenheiten machen zu können. Dasselbe gilt für das Hochladen von Bildern, keine persönliche Daten Dritter zu veröffentlichen.

(7) Für den Fall, dass der/die Nutzer:in gegen die vorstehenden Zusicherungen (§ 3 Abs. 1 bis 6) verstößt, stellt er den Gängeviertel e.V. von sämtlichen Ansprüchen diesbezüglich frei, inklusive der Kosten der Rechtsverteidigung.

(8) Die Nutzer:innen dieser Website können ihr Profil jederzeit löschen lassen, indem Sie eine entsprechende Nachricht mit Nennung der Mailadresse des Profils an widerruf@leerstandsmelder.de schicken. Das Profil der Nutzer:innen und ihre personenbezogenen Daten werden innerhalb von 4 Wochen gelöscht und dies mit einer Mail bestätigt. Die eingestellten Inhalte bleiben unter einem allgemeinen Profil-Pseudonym der Datenbank erhalten.
Bei Inaktivität von länger als 5 Jahren werden die Profil-Daten des Nutzers/der Nutzerin ebenso gelöscht und der Inhalt pseudonymisiert erhalten.

### § 4 – Umgang mit eingestellten Inhalten und Informationen

(1) Leerstandsmeldungen, Fotos und sonstige Angaben, die auf der Website verfügbar sind, stammen vor allem von Nutzer:innen. Es muss berücksichtigt werden, dass ein Beitrag lediglich die Meinung und den Wissensstand der entsprechenden Nutzerin bzw. des entsprechenden Nutzers wiedergibt.

(2) Jeder Versuch einer Störung der korrekten Funktionsweise der Website ist untersagt.

(3) Verstößt ein/e Nutzer:in gegen die hier niedergelegten Bedingungen zur Einstellung von Inhalten/Texten/Bildern ist der Gängeviertel e.V. berechtigt, Inhalte ohne Benachrichtigung des Nutzers/der Nutzerin, insbesondere ohne Angabe von Gründen zu ergänzen, zu ändern oder zu entfernen. Rechtswidrige Beiträge werden umgehend nach Kenntnisnahme gelöscht. In besonderen Fällen wird die Teilnahme eines Nutzers/einer Nutzerin ggf. vollständig ausgeschlossen, seine/ihre eingegebenen Daten und sein/ihr Profil gelöscht.

(4) Die Administratoren haben die Möglichkeit, Leerstandsmeldungen und andere Beiträge (Fotos, Kommentare) zu redigieren. Von dieser Funktion werden sie Gebrauch machen, um Informationen zu vervollständigen oder zu korrigieren oder um Orte zusammenzuführen, wenn sie doppelt im System sind.

(5) Leerstandsmelder respektiert die Immaterialgüterrechte Dritter. Wenn Sie der Auffassung sind, dass Ihre Immaterialgüterrechte in einer Art genutzt wurden, der Anlass zur Befürchtung einer Verletzung gibt, folgen Sie bitte unserem Verfahren zur Mitteilung an hallo(at)leerstandsmelder.de über eine Rechtsverletzung.

(6) Es besteht grundsätzlich kein Anspruch auf Veröffentlichung von Inhalten.

### § 5 – Nutzungsrechte

(1) Durch das Hochladen und Bereitstellen von Texten/Fotos/Inhalten durch die Nutzerin/den Nutzer gewährt diese/r dem Gängeviertel e.V. und den übrigen Nutzer:innen eine unentgeltliche, nicht ausschließliche, übertragbare, zeitlich und örtlich unbeschränkte Lizenz zur Nutzung der von ihr/ihm hochgeladenen Inhalte im Rahmen jetziger oder zukünftiger Produkte oder Dienstleistungen dieser Website. Insbesondere räumt der/die Nutzer:in das Recht ein, die Bilder zu vervielfältigen und auf der Website öffentlich zugänglich zu machen. Das umfasst das Vervielfältigungs-, Verbreitungs- und Übertragungsrecht sowie das Recht zur öffentlichen Wiedergabe, das Recht der öffentlichen Zugänglichmachung, das Senderecht und das Recht der Wiedergabe durch Bild- und Tonträger, ferner das Recht zur Vervielfältigung als Druckwerk. Insbesondere stimmt der/die Nutzer:in einer Übertragung dieser Nutzungsrechte seitens des Gängeviertel e.V. auf Dritte zu. Dies betrifft insbesondere Unternehmen, deren Dienstleistungen sich zur Funktionalität dieser Website bedient wird (OpenStreetMap) und die ebenfalls Nutzungsrechte an den einstellten Inhalten geltend machen.

(2) Die Lizenz umfasst die Berechtigung des Gängeviertel e.V., die Inhalte zu verändern oder verändern zu lassen, soweit dies der Ergänzung und Korrektur sowie der Übertragung in ein anderes Format dient. Der/Die Nutzer:in erklärt sich damit einverstanden, dass seine/ihre Bilder und Texte zur besseren Übersicht umgeschrieben und die Bilder komprimiert werden können. Der/Die Nutzer:in erklärt sich damit einverstanden, dass auch seine/ihre übrigen Inhalte durch den Gängeviertel e.V. bis zu einem gewissen Maß, unter gehöriger Wahrung des Urheberpersönlichkeitsrechts, editiert oder gelöscht oder in andere Sprachen übersetzt werden können. Der/Die Nutzer:in erklärt sich damit einverstanden, dass der Gängeviertel e.V. bei der Verwertung von Texten/Fotos/Inhalten des Nutzers/der Nutzerin nur dessen/deren Profilnamen zur namentlichen Kennzeichnung verwendet. Auf eine weitere Nennung des natürlichen Namens verzichtet der/die Nutzer:in.
Der/Die Nutzer:in erklärt sich damit einverstanden, dass die Inhalte des Leerstandsmelders auch anderen Datenbanken zugefügt und in statistischer Auswertung weiterverarbeitet werden können, um einen Beitrag zur Dokumentation der Entwicklung von Stadt und Region zu leisten.

(3) Die Nutzung durch andere Nutzer:innen ist auf private Zwecke beschränkt. Es ist zulässig, die Leerstandsmeldungen herunterzuladen und auszudrucken. Eine darüber hinausgehende Nutzung ist von der Zustimmung des Gängeviertel e.V. und des Nutzers/der Nutzerin, dessen Inhalte verwendet werden sollen, abhängig.

(4) Der/Die Nutzer:in erklärt hiermit ausdrücklich, dass er/sie für die eingestellten Inhalte sämtliche erforderlichen Rechte besitzt. Dies gilt insbesondere für Urheberrechte an den Fotos und Texten, gewerbliche Schutzrechte und Persönlichkeitsrechte abgebildeter Personen. Soweit der/die Nutzer:in für seine/ihre Leerstandsmeldung Daten und Bilder verwendet, die er/sie nicht vollständig selbst geschaffen sondern von Dritten erhalten oder bei Dritten entnommen hat, mit diesen vor Einstellung auf dieser Website zu klären, ob er/sie die Berechtigung erhält, diese Daten auf der Website einzustellen und zu veröffentlichen. Er/Sie garantiert mit der Einstellung auf der Website, dass er/sie alle erforderlichen Rechtsübertragungen, Lizenzen, Gestattungen, Einwilligungen und dergleichen wirksam eingeholt hat. Für den Fall, dass der/die Nutzer:in gegen diese Zusicherungen verstößt, stellt er/sie den Gängeviertel e.V. von sämtlichen Ansprüchen diesbezüglich frei, inklusive der Kosten der Rechtsverteidigung.

(5) Er/Sie sichert zu, dass er/sie Dritten keine Rechte an Bildern eingeräumt hat bzw. einräumen wird, die der vorliegenden Vereinbarung widersprechen könnten.

(6) Es ist untersagt auf den hochgeladenen Bildern Menschen abzubilden.

(7) Es ist untersagt, auf den hochgeladenen Bildern Innenräume abzubilden.

(8) Für den Fall, dass der/die Nutzer:in gegen die vorstehenden Zusicherungen (§ 5 Abs. 6 bis 7) verstößt, stellt er/sie den Gängeviertel e.V. von sämtlichen Ansprüchen diesbezüglich frei, inklusive der Kosten der Rechtsverteidigung.

### § 6 – Haftung

(1) Der Gängeviertel e.V. übernimmt keine Gewähr für die Vollständigkeit, Aktualität, Korrektheit oder sonstige Qualität der bereitgestellten Informationen. Der Gängeviertel e.V. bietet diese Informationen lediglich als Vermittler an.

(2) Der/Die Nutzer:in muss damit rechnen, dass sich diese Informationen jederzeit ändern können. Dies gilt insbesondere für den Leerstand von Gebäuden. Demzufolge sollte sich der/die Nutzer:in vor Ort, soweit das möglich ist, von der Richtigkeit, Vollständigkeit und Aktualität der Informationen überzeugen.

(3) Darüber hinaus wird darauf hingewiesen, dass die Veröffentlichung von Nutzerinhalten auf der Website keine Äußerung oder Feststellung, Bewertung oder Empfehlung des Gängeviertel e.V. darstellt. Es erfolgt durch diese Veröffentlichung seitens des Gängeviertel e.V. bzw. das Abrufen solcher und auch übriger Inhalte durch den Nutzer/die Nutzerin keine Auskunft und/oder Beratung. Jede/r Nutzer:in erklärt, die Inhalte in eigener Verantwortung zu nutzen.

(4) Der Gängeviertel e.V. übernimmt keine Gewähr für die ununterbrochene Verfügbarkeit von Inhalten/Daten und Informationen. Der Gängeviertel e.V. behält es sich ausdrücklich vor Teile der Seiten, ganze Seiten oder die gesamte Website ohne Ankündigung für einen frei bestimmten Zeitraum zu sperren, zu verändern, zu ergänzen, zu löschen oder die Veröffentlichung zeitweise oder endgültig einzustellen.

(5) Die vorvertragliche, vertragliche und außervertragliche Haftung des Gängeviertel e.V. ist auf Vorsatz und grobe Fahrlässigkeit sowie auf die Verletzung wesentlicher Vertragspflichten beschränkt. Die Haftungsbegrenzung gilt auch für Erfüllungsgehilf:innen.

(6) Die vorstehenden Haftungsbeschränkungen bzw. -ausschlüsse gelten nicht bei schuldhafter Verletzung von Leben, Körper und Gesundheit und für Haftungsansprüche nach dem Produkthaftungsgesetz.

### § 7 – Kennzeichen- und Urheberrecht

(1) Über die in §§ 3 und 4 enthaltenen Nutzungsberechtigungen erfolgt keine Rechteeinräumung hinsichtlich Daten und Inhalte an die Nutzer:innen durch den Gängeviertel e.V.

(2) Die auf den Webseiten, für die diese Nutzungsbedingungen Gültigkeit haben, veröffentlichten Inhalte sind rechtlich geschützt. Vollständige oder teilweise Vervielfältigungen, Verwertung ohne vorherige schriftliche Zustimmung des Gängeviertel e.V. bzw. der einstellenden Nutzer:innen ist nicht gestattet, soweit es sich nicht um Kopien von Inhalten ausschließlich für den persönlichen bzw. nicht gewerblichen Gebrauch handelt.

(3) Die auf der Website veröffentlichten eigenen Inhalte/Daten (z.B. Informationen, Berichte, Bilder, Illustrationen) sind rechtlich geschützt. Jede Art der Vervielfältigung, Verbreitung und sonstigen, insbesondere kommerziellen Verwertung, der öffentlichen Wiedergabe, der Veränderung und/oder Löschung ist untersagt. Dazu zählt auch die Integration und Darstellung von Inhalten/Daten auf fremden Webseiten durch Interlinks, Weblinks, Deep Links oder Frames.

(4) Die Einwilligung zur Verwertung/Nutzung seitens des Gängeviertel e.V. selbst erstellter/ zusammengestellter Inhalte/Daten kann beim Gängeviertel e.V. eingeholt werden.

(5) Die auf der Website abrufbaren Bilder und Beiträge stellen insgesamt eine urheberrechtlich geschützte Datenbank dar. Es ist verboten, diese Datenbank oder wesentliche Teile ohne Zustimmung des Gängeviertel e.V. und außerhalb der hier beschriebenen erlaubten Nutzung von der Website in ihrer Struktur ganz oder auch nur teilweise zu übernehmen oder zu bearbeiten.

### § 8 – Freistellung

(1) Der/Die Nutzer:in stellt den Gängeviertel e.V. von allen Ansprüchen frei, die Dritte ihm/ihr gegenüber auf Grund der vom Nutzer/von der Nutzerin eingetragenen Inhalte und/oder einer nicht vertragsgemäßen, missbräuchlichen und/oder rechtswidrigen Nutzung der Website insbesondere hinsichtlich Verletzungen des Urheberrechts, Persönlichkeitsrechts, Eigentumsrechts oder gewerblicher Schutzrechte Dritter geltend machen, insbesondere die Kosten der Rechtsverteidigung. Der/Die Nutzer:in unterstützt den Gängeviertel e.V. bei der Abwehr solcher Ansprüche, insbesondere stellt er/sie sämtliche zur Verteidigung erforderlichen Informationen zur Verfügung.

(2) Der/Die Nutzer:in ist zum Ersatz des Schadens verpflichtet, der dem Gängeviertel e.V. durch die begründete Inanspruchnahme durch Dritte entstanden ist. Dieser Schadensersatzanspruch umfasst auch die dem Gängeviertel e.V. durch die eigene Rechtsverteidigung entstandenen Kosten.

### § 9 – Datenschutz

(1) Der Gängeviertel e.V. ermöglicht den Nutzer:innen ohne Account eine anonyme Nutzung der Website. In diesem Fall werden lediglich Statistikdaten wie IP-Adresse und Zeitpunkt eines Zugriffs gespeichert.

(2) Personenbezogene Daten für die Erstellung eines Accounts werden von dem/der Nutzer:in mitgeteilt, insbesondere die E-Mail-Adresse, die Nutzung eines Accounts ohne diese Angabe ist jedoch nicht möglich.

### § 10 – Änderung der Nutzungsbedingungen

(1) Diese Allgemeinen Nutzungsbedingungen für die Nutzung der Website gelten für alle Nutzer:innen, die sich ab dem 22.08.2023 registriert haben und/oder die Dienste der Website nutzen.

(2) Im Übrigen behält sich der Gängeviertel e.V. vor, diese Bedingungen mit Wirkung für die Zukunft zu ändern/ ergänzen bzw. zu erneuern, ohne dass eine Pflicht zur Mitteilung an die Nutzer:innen besteht. Falls eine dieser Bedingungen für unwirksam, nichtig oder aus irgendeinem Grund für undurchsetzbar gehalten wird, gilt diese Regelung als abtrennbar und beeinflusst die Gültigkeit und Durchsetzbarkeit aller verbleibenden Regelungen nicht.
Auf den Seiten des Leerstandsmelders wird die jeweils aktuelle Version ab dem Geltungszeitpunkt bereitgehalten. Mit der Weiternutzung der Webseiten erklärt der/die Nutzer:in sein/ihr Einverständnis zu den Änderungen. Es wird empfohlen, die Nutzungsbedingungen regelmäßig auf Änderungen hin zu untersuchen.

### § 11 – Schlussbestimmungen

(1) Es gilt das Recht der Bundesrepublik Deutschland, soweit dem keine international zwingenden gesetzlichen Vorschriften, internationale Übereinkommen und insbesondere international zwingende Verbraucherschutzvorschriften entgegenstehen.

(2) Gerichtsstand für Kaufleute, für Nutzer:innen, die keinen allgemeinen Gerichtsstand im Inland haben, sowie für Nutzer:innen, die nach Abschluss des Vertrages ihren Wohnsitz oder gewöhnlichen Aufenthalt ins Ausland verlegt haben oder deren Wohnsitz oder gewöhnlicher Aufenthalt im Zeitpunkt der Klageerhebung nicht bekannt ist, sowie für Passivprozesse, ist der Sitz des Gängeviertel e.V. in Hamburg. Dies gilt nicht, wenn internationale Übereinkommen oder Verbraucherschutzvorschriften international zwingend etwas anderes vorsehen.

(3) Für sämtliche Rechtsbeziehungen der Parteien gilt das Recht der Bundesrepublik Deutschland. Bei Verbraucher:innen gilt diese Rechtswahl nur insoweit, als nicht der gewährte Schutz durch zwingende Bestimmungen des Rechts des Staates in dem der/die Verbraucher:in seinen/ihren gewöhnlichen Aufenthalt hat, entzogen wird.

(4) Für den Fall, dass eine oder mehrere Bestimmungen dieser Nutzungsbedingungen ganz oder teilweise rechtsunwirksam sind oder werden, wird die Gültigkeit der übrigen Bestimmungen davon nicht berührt.
