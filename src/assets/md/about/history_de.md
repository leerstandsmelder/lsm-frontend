## ÜBER DAS PROJEKT

### Gründung und Geschichte

Das Konzept eines kollektiven Datenpools wurde Ende 2010 vom _[Gängeviertel e.V.](https://das-gaengeviertel.info/)_ in Hamburg entwickelt, um auf die Verschwendung von Ressourcen aufmerksam zu machen und die Debatte um das „Recht auf Stadt“ voranzutreiben. Der Verein ist aus der Initiative _Komm in die Gänge_ hervorgegangen, die 2009 zwölf denkmalgeschützte, aber abrissgefährdete Häuser in der Hamburger Innenstadt besetzte und anschließend den Ankauf der Immobilien durch die Stadt erreichte. Die erste Version des Leerstandsmelder wurde in Kooperation mit Ubilabs entwickelt und auf deren Servern gehostet.

Seit 2012 sind weitere Städte hinzugekommen. Auf Initiative von _[openBerlin e.V](https://www.openberlin.org/)._ gründete sich 2012 der Leerstandsmelder für Berlin und mit dem LEERgang I trafen sich 2013 die Aktiven zum ersten Leerstandsmelder-Kongress in Hamburg. Noch im selben Jahr folgte LEERgang II in Bonn und ein Jahr später LEERgang III in Berlin. 2014 startete Leerstandsmelder Mainz mit einem Kick-off auf der Tagung „[Strategien zur Öffnung und Nutzung von Leerständen](https://www.blogs.uni-mainz.de/fb09kulturgeographie/tagungen-und-konferenzen/konferenz-strategien-zur-oeffnung-und-nutzung-von-leerstaenden-start-von-leerstandsmelder-mainz/)“ (LEERgang IV). Im Jahr 2015 wurde eine Neuentwicklung der Plattform durch eine erfolgreiche Crowdfunding-Kampagne finanziert. Die daraus entstandene Open Source Software wurde mehrere Jahre auf eigenen Servern betrieben. Zu diesem Zeitpunkt zeigte Leerstandsmelder bereits für 29 Städte und Regionen Leerstände in Form einer interaktiven Karte an. Hinter den aktiven Städten steht jeweils eine lokale Gruppe. In Hamburg hat sich 2019 ein neues Team zusammengefunden und auch in Berlin bildete sich eine neue Organisationsgruppe.  
Ein weiterer Versuch einer Neuentwicklung im Jahr 2020 mittels einer öffentlichen Förderung schlug fehl, während ein festes Kernteam weiter zusammenwuchs. Leerstandsmelder wurde inhaltlich überarbeitet und aktualisiert. Darüber hinaus wurden Netzwerke mit Akteur:innen aus dem Themenspektrum Wohnen, Klima, Obdachlosigkeit und grüne Stadtentwicklung initiiert sowie Kooperationen mit gemeinwohlorientierten Zwischennutzungsagenturen, weiteren Initiativen, Mieter:innenvereinen oder zum Beispiel der _[Rosa-Luxemburg-Stiftung](https://www.rosalux.de/)_ aufgebaut.

2020 kamen die Städte Dresden und Krefeld, 2021 Braunschweig und Oberhausen mit ihren lokalen Organisationen hinzu.

Im Januar 2022 konnte LEERgang V digital mit verschiedenen Betreiber:innen stattfinden. Zentrales Thema der Veranstaltung war der Austausch über die aktuelle Situation in den Städten und Regionen und die Frage, wie sich Leerstandsmelder weiterentwickeln muss, um ein effektiveres Instrument einer gemeinwohlorientierten Stadt- und Regionalentwicklungspolitik zu werden.

Ein Fazit war, dass das Thema Wohnen überall an Bedeutung gewonnen hat.

Mit der Förderung durch den _[Prototype Fund](https://prototypefund.de/project/leerstandsmelder-fuer-mehr-transparenz-und-neue-moeglichkeitsraeume/)_ im Jahr 2022 war das Ziel verbunden, die Plattform noch stärker zu einem solchen Werkzeug zu machen: informativ, niedrigschwellig und mobil soll Leerstandsmelder eine Plattform für alle sein! _Der Prototype Fund_ fördert Open-Source-Entwicklungsprojekte im Bereich Civic Technology und ist ein von der _[Open Knowledge Foundation](https://www.okfn.de/)_ betreutes Förderprogramm des _[Bundesministeriums für Bildung und Forschung](https://www.bmbf.de)_.

2022 wurde der Leerstandsmelder Göttingen gegründet und die Plattform nahm an der Bundeskonferenz des _[Netzwerk Mieten & Wohnen](http://www.netzwerk-mieten-wohnen.de/)_ in Berlin teil. Anfang 2023 kommen die Städte Amsterdam und Erlangen hinzu und im Laufe des Jahres geht die neue Website online.

Im Frühjahr 2024 kam es zu einer Kooperation mit der "DIE LINKE" Eimsbüttel. Hierbei wurden über 170 Einträge aus dem Leerstandsmelder überprüft und aktualisiert.
Gemeinsam mit der Initative _[Leben statt Leerstand](https://lebenstattleerstand.de/)_ wurde bei einem Vortrag auf dem _[10. RaS-Forum](https://rechtaufstadt-forum.de/)_ der Leerstandsmelder vorgestellt.

Ab Juli 2024 konnte die Neuentwicklung des Leerstandsmelders finalisiert werden.
Hierfür war eine Förderung durch und Kooperation mit der Initative _[Civic Data Lab](https://civic-data.de/)_ ausschlaggebend. Das Civic Data Lab ist ein gemeinsames Vorhaben der Gesellschaft für Informatik, CorrelAid und dem Deutschen Caritasverband mit dem Ziel zivilgesellschaftliche Datenvorhaben zu unterstützen und zu realisieren.

Im August August 2024, im Rahmen des 15. Geburstags des Gängeviertels, wurde die neue Plattform veröffentlicht. Seit dem ist die Plattform auch unter _[leerstandsmelder.in](https://leerstandsmelder.in)_ erreichbar.

(Stand: August 2024)
