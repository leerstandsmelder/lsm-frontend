import api from "@/utils/api.js";

export default {
  get(params) {
    let url = "/news/";
    return api.get(url, { params });
  },
};
