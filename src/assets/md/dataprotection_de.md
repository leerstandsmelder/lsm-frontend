## Datenschutzerklärung

Hiermit machen wir gemäß unserer Informationspflicht nach Art. 13 Datenschutz-Grundverordnung (DSGVO) Ihnen gegenüber unsere Datenverarbeitungen von personenbezogenen Daten in verständlicher Form transparent.

Personenbezogene Daten sind gemäß Art. 4 Ziff. 1 DSGVO alle Informationen, die sich auf eine bestimmte oder bestimmbare natürliche Person beziehen. Als identifizierbar wird eine natürliche Person angesehen, wenn sie aufgrund der Daten als Einzelperson (wieder)erkannt werden kann.

### 1. Verantwortliche Stelle

Die verantwortliche Stelle für die Datenverarbeitung auf dieser Website ist:

<pre>Verein Gängeviertel e.V.
Vertreten durch den Vorstand
Valentinskamp 28a
20355 Hamburg

</pre>

Zu weiteren Fragen zum Thema personenbezogene Daten können Sie sich jederzeit über die im Impressum aufgeführten Kontaktmöglichkeiten an uns wenden.

Unsere externe Datenschutzbeauftragte ist Beata-Konstanze Hubrig von der Rechtsanwaltskanzlei https://kanzlei-hubrig.de/

### 2. Betroffenenrechte

Sie haben grundsätzlich folgende Rechte:

Auskunftsrecht der betroffenen Person – Art. 15 DSGVO: Sie haben jederzeit im Rahmen der geltenden gesetzlichen Bestimmungen das Recht auf unentgeltliche Auskunft über Ihre gespeicherten personenbezogenen Daten, die Herkunft der Daten, deren Empfänger und den Zweck der Datenverarbeitung

Recht auf Berichtigung – Art. 16 DSGVO: Sie haben jederzeit im Rahmen der geltenden gesetzlichen Bestimmungen das Recht von uns die Berichtigung Sie betreffender unrichtiger personenbezogener Daten zu verlangen.

Recht auf Löschung – Art. 17 DSGVO: Sie haben jederzeit im Rahmen der geltenden gesetzlichen Bestimmungen das Recht von uns die Löschung Ihrer personenbezogenen Daten zu verlangen.

Recht auf Einschränkung der Verarbeitung – Art. 18 DSGVO: Sie haben das Recht, von uns die Einschränkung der Verarbeitung zu verlangen.

Recht auf Datenübertragbarkeit – Art. 20 DSGVO: Ihnen steht das Recht zu, Daten, die wir auf Grundlage Ihrer Einwilligung oder in Erfüllung eines Vertrags automatisiert verarbeiten, an sich oder an Dritte aushändigen zu lassen. Die Bereitstellung erfolgt in einem maschinenlesbaren Format. Sofern Sie die direkte Übertragung der Daten an einen anderen Verantwortlichen verlangen, erfolgt dies nur, soweit es technisch machbar ist.

Widerspruchsrecht – Art. 21 DSGVO: Nur mit Ihrer ausdrücklichen Einwilligung sind einige Vorgänge der Datenverarbeitung möglich. Ein Widerruf Ihrer bereits erteilten Einwilligung ist jederzeit möglich. Sie haben das Recht, aus Gründen, die sich aus ihrer besonderen Situation ergeben, jederzeit gegen die Verarbeitung Sie betreffender personenbezogener Daten, die aufgrund eines berechtigten Interesses unsererseits oder zur Wahrung einer Aufgabe im öffentlichen Interesse erforderlich ist, oder die in Ausübung öffentlicher Gewalt erfolgt, Widerspruch einzulegen. Für den Widerruf genügt eine formlose Mitteilung per E-Mail an widerruf@leerstandsmelder.de. Die Rechtmäßigkeit der bis zum Widerruf erfolgten Datenverarbeitung bleibt vom Widerruf unberührt.

Recht auf Beschwerde bei einer Aufsichtsbehörde – Art. 77 DSGVO i. V. m. § 19 BDSG: Als Betroffener steht Ihnen im Falle eines datenschutzrechtlichen Verstoßes ein Beschwerderecht bei der zuständigen Aufsichtsbehörde zu. Sie haben das Recht, jederzeit Beschwerde bei einer Aufsichtsbehörde, insbesondere in dem Mitgliedstaat ihres Aufenthaltsorts, ihres Arbeitsplatzes oder des Orts des mutmaßlichen Verstoßes, einzulegen, wenn Sie der Ansicht sind, dass die Verarbeitung der sie betreffenden personenbezogenen Daten gegen geltendes Recht verstößt.

### 3. Allgemeine Nutzung der Website

#### 3.1 SSL- bzw. TLS-Verschlüsselung

Aus Sicherheitsgründen und zum Schutz der Übertragung vertraulicher Inhalte, die Sie an uns als Seitenbetreiber senden, nutzt unsere Website eine SSL-bzw. TLS-Verschlüsselung. Damit sind Daten, die Sie über diese Website übermitteln, für Dritte nicht mitlesbar. Sie erkennen eine verschlüsselte Verbindung an der „https://“ Adresszeile Ihres Browsers und am Schloss-Symbol in der Browserzeile.

#### 3.2 Server-Log-Dateien

In Server-Log-Dateien erhebt und speichert der Provider der Website automatisch Informationen, die Ihr Browser automatisch an uns übermittelt.
Dies sind:

    • Browsertyp und Browserversion
    • Verwendetes Betriebssystem
    • Referrer URL
    • Hostname des zugreifenden Rechners
    • Uhrzeit der Serveranfrage
    • IP-Adresse
    • Inhalte auf die zugegriffen wurde sowie
    • die übertragenen Informationen

Es findet keine Zusammenführung dieser Daten mit anderen Datenquellen statt. Grundlage der Datenverarbeitung bildet Art. 6 Abs. 1 lit. f DSGVO, der Betrieb der Webseite ist unser berechtigtes Interesse.

Nehmen Sie unter der auf unserer Website angegebenen E-Mail-Adresse Kontakt zu uns auf, teilen Sie uns zumindest Ihre E-Mail-Adresse mit, sowie gegebenenfalls weitere Informationen, die Sie in Ihrer E-Mail preisgeben. Damit wir Ihr Anliegen bearbeiten können, müssen wir diese Daten verarbeiten. Die Rechtsgrundlage hierfür ist Ihre Einwilligung gemäß Art. 6 Abs. 1 lit. a DSGVO, die Sie jeder Zeit für die Zukunft widerrufen können.

#### 3.3 Zwecke und Rechtsgrundlage der Datenverarbeitung

Die Verarbeitung Ihrer IP-Adresse während des Verbindungsaufbaus erfolgt, damit wir Ihnen unsere Website zur Verfügung stellen können. Sie basiert auf Art. 6 Abs. 1 lit. f) DSGVO. Unser berechtigtes Interesse besteht im genannten Zweck.

Die Verarbeitung im Rahmen einer Kontaktaufnahme erfolgt, damit wir Ihre Anfrage bearbeiten und beantworten könne. Die Rechtsgrundlage ist Art. 6 Abs. 1 lit. a) DSGVO. Unser berechtigtes Interesse liegt im soeben genannten Zweck.

#### 3.4 Datensicherheit

Unsere Webseite und sonstigen Systeme werden, durch technische und organisatorische Maßnahmen gegen Verlust, Zerstörung, Zugriff, Veränderung oder Verbreitung Ihrer Daten durch unbefugte Personen, gesichert. Trotz regelmäßiger Kontrollen ist ein vollständiger Schutz gegen alle Gefahren jedoch nicht möglich.

#### 3.5 Pflicht zur Bereitstellung von Daten

Sie haben keine vertragliche oder gesetzliche Pflicht uns personenbezogene Daten bereitzustellen. Allerdings sind wir ohne die von Ihnen mitgeteilten Daten nicht in der Lage, Ihnen unsere Services anzubieten.

#### 3.6. Bestehen von automatisierten Entscheidungsfindungen (einschließlich Profiling)

Eine automatisierte Entscheidungsfindung findet auf unserer Webseite nicht statt.

### 4. Registrierung auf dieser Website

Zur Nutzung bestimmter Funktionen können Sie sich auf unserer Website registrieren. Dafür benötigen wir von Ihnen eine E-Mail-Adresse sowie Informationen unserer Überprüfung, dass Sie der Inhaber der angegebenen E-Mail-Adresse sind und mit den Nutzungsbedingungen von Leerstandsmelder einverstanden sind (Bestätigungs-E-mail). Die übermittelten Daten dienen ausschließlich zum Zwecke der Nutzung des jeweiligen Angebotes oder Dienstes und werden nicht an Dritte weitergegeben.

Im Falle wichtiger Änderungen, etwa aus technischen Gründen, informieren wir Sie per E-Mail. Die E-Mail wird an die Adresse versendet, die bei der Registrierung angegeben wurde.

Die Verarbeitung der bei der Registrierung eingegebenen Daten erfolgt auf Grundlage Ihrer Einwilligung (Art. 6 Abs. 1 lit. a DSGVO). Ein Widerruf Ihrer Einwilligung ist jederzeit möglich. Für den Widerruf genügt eine formlose Mitteilung per E-Mail an widerruf@leerstandsmelder.de. Die Rechtmäßigkeit der bereits erfolgten Datenverarbeitung bleibt vom Widerruf unberührt.

Wir speichern die bei der Registrierung erfassten personenbezogenen Daten während des Zeitraums, den Sie auf unserer Website registriert sind. Ihre personenbezogenen Daten werden gelöscht, sollten Sie Ihre Registrierung aufheben. Gesetzliche Aufbewahrungsfristen bleiben unberührt.

### 5. Speicherdauer

Beiträge und Kommentare sowie damit in Verbindung stehende Daten, wie beispielsweise IP-Adressen, werden gespeichert. Der Inhalt verbleibt auf unserer Website, bis er vollständig gelöscht wurde oder aus rechtlichen Gründen gelöscht werden musste.

Bei inaktiven Nutzer:innen-Profilen werden die personenbezogenen Daten ebenfalls nach 5 Jahren Inaktivität gelöscht.

Die Speicherung der Beiträge und Kommentare erfolgt auf Grundlage Ihrer Einwilligung (Art. 6 Abs. 1 lit. a DSGVO). Ein Widerruf Ihrer Einwilligung ist jederzeit für die Zukunft möglich. Für den Widerruf genügt eine formlose Mitteilung per E-Mail an widerruf@leerstandsmelder.de. Die Rechtmäßigkeit bereits erfolgter Datenverarbeitungsvorgänge bleibt vom Widerruf unberührt.

Ihre E-Mails und Kontaktaufnahmen speichern wir so lange, wie es zur Bearbeitung Ihrer Anfrage erforderlich ist und speichern Sie anschließend für einen Zeitraum von 2 Jahren, falls Sie sich bezugnehmend auf Ihre ursprüngliche Frage noch einmal an uns wenden.

### 6. Weiter Internetspezifische Datenverarbeitungen (Cookies)

Unsere Website verwendet Cookies. Das sind kleine Textdateien, die Ihr Webbrowser auf Ihrem Endgerät speichert. Cookies helfen uns dabei, unser Angebot nutzerfreundlicher, effektiver und sicherer zu machen.

Unsere Cookies sind “Session-Cookies.” Solche Cookies werden nach Ende Ihrer Browser-Sitzung von selbst gelöscht.

Mit einem modernen Webbrowser können Sie das Setzen von Cookies überwachen, einschränken oder unterbinden. Viele Webbrowser lassen sich so konfigurieren, dass Cookies mit dem Schließen des Programms von selbst gelöscht werden.

Das Setzen von Cookies, die zur Ausübung elektronischer Kommunikationsvorgänge oder der Bereitstellung bestimmter, von Ihnen erwünschter Funktionen notwendig sind, erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Als Betreiber dieser Website haben wir ein berechtigtes Interesse an der Speicherung von Cookies zur technisch fehlerfreien und reibungslosen Bereitstellung unserer Dienste.

### 7. Meldung Leerstand

Für die Leerstandsmeldung auf dieser Seite werden neben Ihrer Meldung auch Angaben zum Zeitpunkt der Erstellung des Eintrags, Ihre E-Mail-Adresse und der von Ihnen gewählte Nutzername gespeichert.

Unsere Eintragsfunktion speichert die IP-Adressen der Nutzer:innen, die Meldungen verfassen und diese kommentieren und ergänzen. Da wir Einträge auf unserer Seite nicht vor der Freischaltung prüfen, benötigen wir diese Daten, um im Falle von Rechtsverletzungen wie Beleidigungen oder Propaganda gegen den Verfasser vorgehen zu können.

Die Einträge und die damit verbundenen Daten (z.B. IP-Adresse) werden gespeichert und verbleiben auf unserer Website, bis der Inhalt vollständig gelöscht wurde oder aus rechtlichen Gründen gelöscht werden müssen (z.B. beleidigende Kommentare).

Die Speicherung der Einträge erfolgt auf Grundlage Ihrer Einwilligung (Art. 6 Abs. 1 lit. a DSGVO). Sie können eine von Ihnen erteilte Einwilligung jederzeit widerrufen. Dazu reicht eine formlose Mitteilung per E-Mail an widerruf@leerstandsmelder.de. Die Rechtmäßigkeit der bereits erfolgten Datenverarbeitungsvorgänge bleibt vom Widerruf unberührt.

### 8. Anmerkungen oder Fragen

Wir treffen alle uns möglichen Vorkehrungen zum Schutz und zur Sicherheit Ihrer Daten. Ihre Fragen und Kommentare zum Datenschutz sind uns willkommen, schreiben Sie einfach eine Mail an hallo@leerstandsmelder.de

**Hamburg, August 2023**
