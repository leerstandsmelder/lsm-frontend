// Pinia Store
import { defineStore, acceptHMRUpdate } from "pinia";
import { RepositoryFactory } from "@/utils/repositories/repositoryFactory";
import { useRoute } from "vue-router";
import { toRefs, unref, ref, toValue } from "vue";

export const useNewsStore = defineStore({
  id: "news",
  state: () => ({
    status: "initial",
    news: [],
    selection: "",
    start: 0,
    limit: 5,
  }),
  getters: {
    newsStatus: (state) => state.status,
    article: (state) => {
      const route = useRoute() || { params: { slug: "" } };
      if (state.news.length) {
        if (state.selection || route.params.slug) {
          let ar = state.news.find(
            (x) => x.id == state.selection || x.slug == route.params.slug
          );
          return ar;
        } else {
          return state.news[0];
        }
      } else {
        return {};
      }
    },
  },
  actions: {
    load() {
      if (this.status == "initial" || this.status == undefined) {
        this.fetchNews();
      }
    },
    async fetchNews(params = {}) {
      try {
        let defaults = { limit: this.limit, start: this.start };
        params = { ...defaults, ...params };
        this.status = "loading";
        const NewsRepository = RepositoryFactory.get("news");
        let news = await NewsRepository.get(params);
        this.news = [...news];
        this.status = "loaded";
      } catch (error) {
        this.status = "error";
      }
    },
    select(id) {
      this.selection = id;
    },
    raiseLimit() {
      this.start = 0;
      this.limit = this.limit + 20;
    },
    clear() {
      this.$reset();
    },
  },
  persist: false,
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useNewsStore, import.meta.hot));
}
