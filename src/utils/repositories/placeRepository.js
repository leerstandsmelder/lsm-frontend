import api from "@/utils/api.js";
import { useRegionStore } from "@/stores/region";

export default {
  get(slug = "", params = {}) {
    return api.get(`places/${slug}`, { params });
  },
  getGeoJson(slug = "", params = {}) {
    return api.get(`places.geojson/${slug}`, { params });
  },
  create(params) {
    const regionStore = useRegionStore();
    let region = regionStore.getregionData;
    params["layer_id"] = region.default_layer;
    let url =
      "/maps/" + region.id + "/layers/" + region.default_layer + "/places/";
    return api.post(url, { place: params });
  },
  update(slug, params) {
    return api.put(`places/${slug}`, { place: params });
  },
  destroy(slug) {
    return api.delete(`places/${slug}`);
  },
};
