## HILFE + SUPPORT

Die Inhalte stellen keine Rechtsberatung dar, es handelt sich lediglich um eine allgemeine Darstellung der Rechtslage, die nicht auf Einzelfälle eingeht.

Für einfacheren barrierefreien Zugang wird auf Leerstandsmelder die Gender-Form mit Doppelpunkt verwendet.
Diese Personenbezeichnungen beziehen sich aber ausdrücklich– sofern nicht anders kenntlich gemacht – auf alle existierenden Identitäten.

### GUT ZU WISSEN

#### Was ist Zweckentfremdung?

Zweckentfremdung ist ein vorwiegend juristisch gebrauchter Begriff, der eine nicht vorgesehene oder unerlaubte Nutzung von Sachen bezeichnet.  
Eine zweckfremde Nutzung von Wohnraum ist beispielsweise die Nutzung als Gewerbe oder Ferienwohnung; ebenso wie bauliche Veränderungen zur Nichteignung von Wohnraum, Leerstand, sowie Abriss. Zum Schutz von Wohnraum gibt es in vielen Bundesländern Vorschriften gegen die Zweckentfremdung. Die Festlegung des Zeitraums für genehmigungsfreien Leerstand ist je nach Bundesland im Wohnraumschutzgesetz, Wohnraumstärkungsgesetz, Zweckentfremdungsverbotsgesetz, usw. geregelt. Weiter Infos in [Rechtliche Lage](/site/definition/legal_de) und [Fristen der Städte und Regionen](/site/definition/term_de)

#### Was ist das Zweckentfremdungsverbotsgesetz, das Wohnraumschutzgesetz oder das Wohnraumstärkungsgesetz?

Für die Regelungen zum Schutz von Wohnraum gibt es in verschiedenen Bundesländern unterschiedliche Bezeichnungen; ebenso ist die Festlegung des Zeitraums für genehmigungsfreien Leerstand je nach Bundesland unterschiedlich. Weiter Infos in [Fristen der Städte und Regionen](/site/definition/term_de)

#### Was ist spekulativer Leerstand?

Als spekulativ wird Leerstand bezeichnet, wenn Wohneinheiten trotz Nutzungsmöglichkeit und Nachfrage nicht vermietet oder verkauft werden, weil die Eigentümer:innen auf eine höhere Mietrendite oder einen höheren Verkaufserlös spekulieren; spekulativer Leerstand ist also die künstliche Verknappung des Angebots zu Kapitalisierungszwecken. Leerstand wird auch methodisch im Zuge von Sanierung, Umbau und Verkauf eingesetzt. Dabei werden häufig Altmieter:innen gezielt [entmietet](/site/support/goodtoknow_de#was-ist-entmietung), da Leerverkauf und Neuvermietung die Rendite erhöhen. Mehr info in [Was ist Leerstand](/site/definition/definition_de)

#### Was ist die Leerstandsquote?

Die Leerstandsquote setzt die Zahl der leer stehenden Wohnungen ins Verhältnis zum gesamten Wohnungsbestand, wobei nur die marktaktiven Leerstände im Geschosswohnungsbereich (ohne Einfamilienhäuser) berücksichtigt werden. Marktaktiv bedeutet, dass die Wohnungen sofort wieder bezogen werden könnten (im Gegensatz zu totaler Leerstand).  
Das Ausmaß des Leerstandes steht in direktem Zusammenhang mit den möglichen Erträgen aus vermieteten Immobilien; je geringer der Leerstand, desto höher wird die Wirtschaftlichkeit und Rentabilität eingeschätzt. Mehr info in [Was ist Leerstand](/site/definition/definition_de)

#### Was ist marktaktiver Leerstand?

Als marktaktiver Leerstand werden Wohnungen, „die unmittelbar vermietbar oder mittelfristig aktivierbar sind", bezeichnet. Im Gegensatz zu dysfunktionalem Leerstand (_Problemimmobilien_, brachliegende Dauerbaustellen, Liegenschaften in juristischen Verfahren wie z.B. bei Erbstreitigkeiten usw.). Die offizielle Leerstandsquote beinhaltet nur marktaktive Leerstände von Geschosswohnungen (ohne Einfamilienhäuser). Mehr info in [Was ist Leerstand](/site/definition/definition_de)

#### Was ist totaler Leerstand?

Der Begriff totaler Leerstand umfasst die Summe von marktaktivem Leerstand und nicht marktaktivem Leerstand von Geschosswohnungen sowie Einfamilienhäusern. Das Statistische Bundesamt ermittelt totalen Leerstand auf Basis des Zensus.

#### Was ist Zwischennutzung/temporäre Nutzung?

Zwischennutzung ist eine zeitlich begrenzte Nutzung von Räumen oder Flächen. Meist sind Zwischennutzungen mit Nutzungsänderungen und Vorzugskonditionen gegenüber der vorherigen Nutzung verbunden. Ob soziale, kulturelle oder wirtschaftliche Zwischennutzungen, sie ermöglichen oftmals die Umsetzung von Projekten oder Ideen, die zu marktüblichen Mietpreisen nicht realisierbar wären. Häufig werden Zwischennutzungen auch als Instrument der Stadtentwicklung eingesetzt, um Leerstände zu beleben und das städtische Miteinander zu stärken. Zwischennutzung ist oft der kleinste gemeinsame Nenner zwischen Eigentümer:innen und Nutzer:innen und kann das Ausprobieren neuer Ideen erleichtern oder Ausgangspunkt für eine langfristig neue Art der Nutzung werden. Zwischennutzungen können aber auch temporär ein soziales, kulturelles oder gemeinwohlorientiertes Anliegen vortäuschen.

#### Was ist eine Hausbesetzung?

Hausbesetzungen haben häufig das Ziel, Wohnraum zu sichern und vor Abriss zu schützen, sowie Freiräume zu schaffen und zu erhalten. Damit richten sich Hausbesetzungen oftmals gegen eine Stadt- und Regionalentwicklung, die das Gemeinwohl in den Hintergrund drängt. Die Besetzung eines leerstehenden Gebäudes ist eine rechtlich unzulässige Inbesitznahme, da sie gegen den Willen des Eigentümers bzw. der Eigentümerin verstößt. Dieser Rechtsbruch wird von den Besetzer:innen bewusst in Kauf genommen. Es gibt aber auch Fälle von geduldeten Hausbesetzungen. Einige Hausbesetzungen der 1970er, 1980er und 1990er Jahre sind inzwischen legalisiert. Das bedeutet, dass die Besetzer:innen Duldungs-, Miet- oder Nutzungsverträge mit den Eigentümer:innen abgeschlossen bzw. die Häuser erworben haben. Einige Mietverhältnisse sind nicht formal legalisiert, sondern haben einen inoffiziellen Duldungsstatus. Weitere Informationen bei [Wikipedia/Hausbesetzung](https://de.wikipedia.org/wiki/Hausbesetzung) oder in der Magisterarbeit 2011 über das _Gängeviertel_ [hier](http://das-gaengeviertel.info/fileadmin/media/texte/Magisterarbeit_Schuldt.pdf)

#### Was ist graue Energie?

Die graue Energie eines Produktes entspricht dem gesamten Energieaufwand für Herstellung, Transport, Lagerung, Verkauf und Entsorgung, das heißt dem gesamten nicht erneuerbaren Energieaufwand für Bau, Unterhalt und Rückbau eines Gebäudes. Je nach Materialwahl und Materialeinsatz ist der Anteil an grauer Energie erheblich. Ein minimaler Einsatz von grauer Energie ist gewährleistet, wenn die technische Lebensdauer der Baustoffe maximal ausgenutzt wird. Die Bilanz der grauen Energie fällt daher bei einem Altbau deutlich besser aus als bei einem Neubau. Mehr Informationen beim _[Abrissmoratorium](https://abrissmoratorium.de/Glossar)_

#### In welchem Verhältnis steht der Abriss von Gebäuden zum Leerstand?

Abriss bezeichnet im Bauwesen die vollständige oder teilweise Zerstörung und Beseitigung von Bauwerken aller Art. Der Abriss ist Schicksal und Ziel vieler langjähriger Leerstände. Nach jahrelangem Leerstand und Verfall sind die Gebäude baufällig, von Pilzen angegriffen und kaum noch zu retten. Wenn „endlich“ abgerissen werden darf, können Eigentümer:innen freier über ihre Flächen verfügen, teure Denkmalschutzbestimmungen lösen sich damit auf und mit dem folgenden Neubau lassen sich höhere Renditen erzielen. Die Systematik der Praxis zeigt sich in den Geschichten von dokumentierten _Problemimmobilien_. Weiter Informationen beim [Abrissmoratorium](https://abrissmoratorium.de/)

#### Was ist das Abriss-Moratorium?

„2021 wurden rund 14.090 Gebäudeabrisse statistisch erfasst. […] Recherchen des Bundesarbeitskreises Wohnungsmarktbeobachtung vom _[Bundesinstitut für Bau-, Stadt- und Raumforschung BBSR](https://www.bbsr.bund.de)_ deuten jedoch darauf hin, dass im Bundesdurchschnitt lediglich ein Viertel der tatsächlichen Verluste erfasst werden. Dies liegt daran, dass ein großer Teil der Abrisse nicht genehmigungs- sondern nur anzeigepflichtig sind, und dieser Anzeigepflicht nicht nachgekommen wird. […]  
Im Fall des Abriss-Moratoriums ist ein Aufschieben von Abrissaktivitäten bis zur Schaffung einer gesetzlich geregelten Genehmigungspflicht für Abrisse unter Maßgabe des Gemeinwohls, also der Prüfung der sozialen und ökologischen Umweltwirkungen gemeint.“

Das Abriss-Moratorium bildet ein Element im komplexen Zusammenspiel verschiedener Maßnahmen für eine Bauwende, die mit Initiativen wie der [MusterUMbauordnung](https://www.architects4future.de/wissen/musterumbauordnung-vorschlage-a4f) von _[Architects for Future](https://www.architects4future.de/)_, der [Charta von Rom 2022](https://bauhausearthbackend.com/wp-content/uploads/2022/06/Charta_DE.pdf) von _[Bauhaus Earth](https://www.bauhauserde.org)_, dem _[Haus der Erde des BDA](https://www.bda-bund.de/2019/08/das-haus-der-erde_bda-position/)_ und anderen formuliert werden.
(Quelle: _[Abrissmoratorium](https://abrissmoratorium.de/)_)

Leerstandsmelder unterstützt die Forderungen des Abriss-Moratoriums!

#### Was ist Geldwäsche?

Unter Geldwäsche versteht man finanzielle Transaktionen, die darauf abzielen, die Herkunft und die Existenz von Geld oder anderen Vermögenswerten aus illegalen Transaktionen zu verschleiern, um sie anschließend wieder in den legalen Wirtschaftskreislauf einzubringen.
Ziel der Geldwäsche ist es, die Herkunft illegal erworbener Gelder zu verschleiern. In Deutschland werden jährlich schätzungsweise 100 Milliarden Euro zu „sauberem” Geld gewaschen, ein Großteil davon auf dem intransparenten deutschen Immobilienmarkt. (Quelle: [_Correctiv_ 2022](https://correctiv.org/aktuelles/wem-gehoert-die-stadt/2020/07/15/intransparenz-geldwaesche-steuern-gesetz-oase-muenchen-immobilien-wem-gehoert/))

Am 14.11.2019 hat der Bundestag neue Regeln gegen Geldwäsche beschlossen. Das Gesetz erweitert die Pflichten zur Geldwäscheprävention auch im Immobilienbereich, unter anderem durch die Erfassung von öffentlichen Versteigerungen und Änderungen bei der Verdachtsmeldepflicht.
Von besonderer Bedeutung ist, dass die wirtschaftlich Berechtigten ausländischer Gesellschaften, die Immobilien erwerben, in das [Transparenzregister](https://www.transparenzregister.de) eingetragen werden müssen. Andernfalls dürfen Notare solche Geschäfte nicht mehr beurkunden.

Weitere Informationen in der [Recherche](https://correctiv.org/aktuelles/wem-gehoert-die-stadt/2020/07/15/intransparenz-geldwaesche-steuern-gesetz-oase-muenchen-immobilien-wem-gehoert/) von Michel Penke 2020 für _[Correctiv](https://correctiv.org/)_ oder im Glossar der _[#200Häuser](https://www.200haeuser.de/informationen-glossar/#geldw%C3%A4sche)_

#### Warum braucht es Transparenz auf dem Immobilienmarkt?

Von mehr Transparenz profitieren alle: Die Bewohner:innen wissen, in welchen Händen ihre Wohnung ist. Journalist:innen gelangen an Informationen, um auf Missstände hinzuweisen und die Stadt erhält einen wichtigen Überblick über die Eigentumsverhältnisse. Die Diskussion um die Gestaltung einer lebenswerten Stadt kann auf einer transparenten Grundlage zielgerichteter geführt werden. Kurz: Informationen zur Stadtentwicklung sollten für alle zugänglich sein. Siehe [Was ist das Transparenzregister](/site/support/goodtoknow_de#was-ist-das-transparenzregister)

Der deutsche Immobilienmarkt ist jedoch intransparent wie kaum eine andere Branche. Weder sind Informationen für die Zivilgesellschaft zugänglich, noch kennen die Kommunen den Markt. Der Inhalt des [Grundbuchs](/site/support/goodtoknow_de#was-steht-im-grundbuch) ist ein gut gehütetes Geheimnis. In einigen EU-Ländern sowie in U.K. ist es dagegen üblich, dass die Eigentumsverhältnisse öffentlich einsehbar sind.

Intransparenz begünstigt Kriminalität, undurchsichtige Firmengeflechte, verdeckte Verkäufe und Geldwäsche. Die Möglichkeit, Geld anonym anzulegen oder Gewinne an der Steuer vorbei zu verschieben, ist gängige Praxis.

Nach ersten Rechercheprojekten von _[CORRECTIV](https://correctiv.org)_ in Hamburg und Berlin gab es bereits Vorstöße von Oppositionsparteien im Bundestag für ein offenes Immobilienregister, in dem zumindest die Firmen als Eigentümer:innen und damit auch die tatsächlichen Eigentümer:innen einsehbar sein sollten. Einige große Eigentümer:innen setzen sich auch für mehr Transparenz ein. Die Große Koalition hat diese Vorstöße jedoch blockiert.
Mehr Informationen dazu [hier](https://correctiv.org/aktuelles/wem-gehoert-die-stadt/2020/07/15/intransparenz-geldwaesche-steuern-gesetz-oase-muenchen-immobilien-wem-gehoert/)
Mehr zum Thema Transparenz auf dem Immobilienmarkt [hier](https://correctiv.org/themen/wem-gehoert-die-stadt/)

#### Was ist das Transparenzregister?

Das im Geldwäschegesetz [GwG](https://www.gesetze-im-internet.de/gwg_2017/) in den §§ 18 ff. verankerte [Transparenzregister](https://www.transparenzregister.de) ist ein auf einer EU-Richtlinie basierendes und in nationales Recht umgesetztes Register zur zentralen Erfassung durch nationale und europäische Partnerbehörden. Die Meldepflicht bezieht sich auf die in [§ 19 GwG](https://www.gesetze-im-internet.de/gwg_2017/__19.html) genannten Daten bzw. Datenänderungen der wirtschaftlich Berechtigten von juristischen Personen des Privatrechts und eingetragenen Personengesellschaften. Das Transparenzregister soll der Verhinderung von Geldwäsche und Terrorismusfinanzierung dienen, gilt jedoch als wenig ergiebig bei der Ermittlung der wahren Eigentümer:innen von Immobilien. Mehr Information in der Studie [„Keine Transparenz trotz Transparenzregister“](https://www.rosalux.de/fileadmin/rls_uploads/pdfs/Studien/Studien_5-20_Transparenz_2.Aufl_web.pdf)

Weitere Informationen im Glossar der _[#200Häuser](https://www.200haeuser.de/informationen-glossar/#transparenzregister)_, bei [_Wikipedia_/Transparenzregister](https://de.wikipedia.org/wiki/Transparenzregister) oder im [Transparenzregister](https://www.transparenzregister.de) direkt.

#### Was sind Share Deals?

Größere Immobilientransaktionen werden häufig als Share Deal abgewickelt. Bei einem Share Deal (Anteilskauf) erwerben die Käufer:innen Anteile (Shares) an einem Unternehmen. Zu diesem Zweck werden u.a. Immobilien in Form einer Kapitalgesellschaft gebündelt. Rechtlich handelt es sich beim Kauf von bis zu 90 Prozent der Anteile an einer solchen Gesellschaft nicht um den Kauf einer Immobilie, sondern um den Kauf eines Unternehmens bzw. einer Unternehmensbeteiligung (in Form von Aktien, Geschäftsanteilen oder Kapitalanteilen). Damit entfällt bei einem Share Deal die Grunderwerbsteuer, die in Deutschland je nach Bundesland zwischen 3,5 und 6,5 Prozent des Kaufpreises für ein unbebautes oder bebautes Grundstück beträgt.

Durch Share Deals entgehen dem deutschen Staat jährlich mehr als eine Milliarde Euro an Steuereinnahmen aus Immobilienverkäufen.  
In Jahren 2018 bis 2021 wurden beim Kauf von mindestens 150.000 Wohnungen Share Deals unter anderem von bekannten großen Wohnungskonzernen in Deutschland genutzt, so die Antwort der Bundesregierung auf eine [kleine Anfrage](https://dserver.bundestag.de/btd/19/324/1932469.pdf).  
Von 1999 bis 2016 wurden bei 71 Prozent der gehandelten Wohnungen Share Deals eingesetzt. Berücksichtigt wurden jeweils nur Transaktionen von Portfolios mit mehr als 800 Wohnungen, so eine weitere Antwort der Bundesregierung auf eine [kleine Anfrage](https://dserver.bundestag.de/btd/18/119/1811919.pdf).

Die Studie [„Wem gehört die Stadt“](https://www.rosalux.de/fileadmin/rls_uploads/pdfs/Studien/Studien_13-20_Wem_gehoert_die_Stadt.pdf)
von Christoph Trautvetter beschäftigt sich ausführlich mit dem Thema.
Ebenso sehr informativ [„Wie Immobilienkonzerne den Fiskus prellen“](https://www.deutschlandfunkkultur.de/share-deals-wie-immobilienkonzerne-den-fiskus-prellen-100.html) von _DeutschlandfukKultur_ und
[„Wie Steuerprivilegien die Wohnungsnot vergrößern“](https://www.investigate-europe.eu/de/2022/steuerprivilegien-wohnungsnot-vergrossern/) von _Investigate Europe_.
Weiterführende Informationen beim _[Netzwerk Steuergerechtigkeit](https://www.netzwerk-steuergerechtigkeit.de/)_

#### Was ist ein Katasteramt?

Katasterämter sind für die Vermessung aller Grundstücke und der darauf befindlichen Gebäude in einer Gemeinde zuständig. Die Kombination von Grundstück und Gebäude wird als „Liegenschaft“ bezeichnet. Die Flurstücksnummer wird zusammen mit dem Namen des Eigentümers oder der Eigentümerin im Liegenschaftskataster geführt. Die _[AdV](https://www.adv-online.de/Startseite/) (Arbeitsgemeinschaft der Vermessungsverwaltungen der Länder der Bundesrepublik Deutschland)_ koordiniert das amtliche deutsche Vermessungswesen bundesweit. Darüber hinaus gibt es in jeder Region örtlich zuständige Katasterämter.

#### Was ist der Bodenrichtwert?

Der Bodenrichtwert ist ein durchschnittlicher Lagewert zur Bewertung von Grundstücken. Er wird in Euro pro Quadratmeter angegeben und wird zum Beispiel für die Berechnung der Grundsteuer verwendet. Die Gutachterausschüsse ermitteln die Bodenrichtwerte auf der Grundlage von Kaufpreissammlungen vergangener Grundstücksverkäufe.

#### Was ist ein Flurstück?

Ein Flurstück ist eine amtlich vermessene, abgegrenzte und eindeutig bezeichnete Fläche. Ein Grundstück besteht aus mindestens einem Flurstück und wird durch die Flurstücksnummer eindeutig bezeichnet.
Die Flurstücksnummer einer Wohnung oder einer Gewerbeeinheit ist gemeinsam mit den Namen der Eigentümer:innen im Liegenschaftskataster, im Grundbuch und ggf. im Notarvertrag verzeichnet.

#### Was steht im Grundbuch?

„Das Grundbuch ist ein beschränkt öffentliches Register, in welchem die Grundstücke, grundstücksgleiche Rechte, die hieran bestehenden Eigentumsverhältnisse und die damit verbundenen Rechte und Belastungen verzeichnet sind. […] Sie sind ein mehr oder weniger vollständiges Verzeichnis aller in einem Bezirk vorhandenen Grundstücke und deren Eigentümer.“
Quelle: _[Wikipedia](https://de.wikipedia.org/wiki/Grundbuch)_. Ebenso wird darin verzeichnet, wenn eine Person ein lebens­langes Wohn­recht hat, ob Leitungen über ein Grund­stück verlegt werden dürfen oder es mit Hypotheken belastet ist. Mehr Informationen [hier](https://www.200haeuser.de/informationen-glossar/)

Jede Person, die ein „berechtigtes Interesse“ hat und dieses auch nachweisen kann, also z.B. Mieter:innen, Erb:innen, potenzielle Kreditgeber:innen oder Kaufinteressent:innen dürfen grundsätzlich das Grundbuch einer Immobilie oder eines Grundstückes kostenfrei einsehen ([§ 12 Grundbuchordnung](https://www.gesetze-im-internet.de/gbo/__12.html)).
Die Grundbuchämter sind weder auf Landes– noch Bundesebene miteinander vernetzt – dies wäre aber zum Beispiel notwendig, um professionelle Geldwäsche aufzudecken oder Besitzverhältnisse großer Immobilienkonzerne zu erfassen (sofern diese von sich aus nicht transparent agieren).

#### Was ist eine natürliche Person?

Jeder Mensch ist im rechtlichen Sinne eine sogenannte natürliche Person. Im Gegensatz zur natürlichen Person steht die juristische Person.
Die personenbezogenen Daten natürlicher Personen sind durch die [DSGVO](https://dsgvo-gesetz.de/) geschützt. Leerstandsmelder sammelt keine Informationen über natürliche Personen.

#### Was ist eine juristische Person?

Eine juristische Person ist keine Einzelperson, sondern eine Gruppe von Personen, die durch das Gesetz als rechtlich selbständig anerkannt wird. Juristische Person ist ein Rechtsbegriff für unterschiedliche Rechtsformen. Dazu gehören z.B. eingetragene Vereine, Aktiengesellschaften, Verbände oder Stiftungen. Die Unterteilung in weitere Unterformen definiert die Rechte und Pflichten der jeweiligen Gesellschaft.

- Personengesellschaft
- Kapitalgesellschaft
- Mischformen aus den vorgenannten Gesellschaftsformen

#### Was ist eine Kapitalgesellschaft?

Eine Kapitalgesellschaft ist eine Körperschaft des Privatrechts und damit eine juristische Person, deren Gesellschafter:innen einen gemeinsamen, meist wirtschaftlichen Zweck verfolgen. Bei der Kapitalgesellschaft steht das Kapital im Vordergrund, das die Gesellschafter:innen einbringen. Die Haftung ist auf diese Kapitaleinlage beschränkt, dadurch minimiert sich das unternehmerische Risiko.  
Rechtsformen einschließlich Mischformen:  
AG | gAG | GmbH | gGmbH | InvAG | KGaA | AG & Co. KGaA | SE & Co. KGaA | GmbH & Co. KGaA | Stiftung & Co. KGaA | REIT-AG | UG – haftungsbeschränkt | Limited | SARL |  
SE (Rechtsform nach EU-Recht)

#### Was ist eine Personengesellschaft?

Die Personengesellschaft ist der Zusammenschluss von mindestens zwei Rechtsträgern zu einer Unternehmergesellschaft. Dabei kann es sich um juristische Personen, natürliche Personen oder eine andere Personengesellschaft handeln.
Personengesellschaften besitzen im Gegensatz zu Kapitalgesellschaften keine eigene Rechtspersönlichkeit, sind aber Träger von Rechten und Pflichten. Sie haften im Gegensatz zu Kapitalgesellschaften unbeschränkt mit ihrem Privatvermögen. Der Zusammenschluss dient der Verfolgung eines bestimmten Zwecks, der nicht notwendigerweise wirtschaftlicher Natur sein muss.  
Rechtsform einschließlich Mischformen:  
GbR | KG | AG & Co. KG | GmbH & Co. KG | Limited & Co. KG | Stiftung & Co. KG | Stiftung GmbH & Co. KG | UG (haftungsbeschränkt) & Co. KG | OHG | GmbH & Co OHG | AG & Co OHG | Partenreederei | PartG | PartG mbB | Stille Gesellschaft |  
EWIV (Rechtsform nach EU-Recht)

#### Wer ist juristisch ein Eigentümer oder eine Eigentümerin ?

Eigentümer:innen sind die wirtschaftlich Berechtigten, also diejenigen, denen die Sache rechtlich gehört (vgl. [§ 903 BGB](https://www.gesetze-im-internet.de/bgb/__903.html)). Bei einem Grundstück ist Eigentümer:in, wer als solcher im Grundbuch eingetragen ist. Ist eine Person als Eigentümer:in im Grundbuch eingetragen, so vermutet das Gesetz, dass ihr das Recht zusteht (öffentlicher Glaube des Grundbuchs, vgl. [§ 891 BGB](https://www.gesetze-im-internet.de/bgb/__891.html)).
Das Eigentum unterscheidet sich vom bloßen Besitz. Besitz bedeutet die tatsächliche Sachherrschaft einer Person (vgl. [§ 854 BGB](https://www.gesetze-im-internet.de/bgb/__854.html)). Besitzer:in ist also die Person, die das Grundstück, das Haus oder die Wohnung bewohnt (z.B. Mieter:in).

#### Wer sind öffentliche Eigentümer?

Öffentliche Eigentümer sind z.B. Staat, Bundesland, Stadt, Kommune und Kirche. Diese „Organisationseinheiten“ werden Kraft Gesetzes oder einer Satzung gegründet, nehmen Aufgaben der Daseinsvorsorge wahr und werden als Gebietskörperschaft bezeichnet.
Alle Bürger:innen mit Wohnsitz auf dem jeweiligen Gebiet sind (Pflicht-)Mitglieder der Gebietskörperschaft und durch ihr Wahlrecht grundsätzlich beteiligt. Das Eigentum an Liegenschaften von Bund, Ländern, Kommunen Städten und Kirchen umfasst Gebäude, Grundstücke und Wälder in ganz Deutschland.  
Der Begriff "öffentliches Eigentum" ist nur vage definiert, wird oft synonym benutzt mit Begriffen wie Gemeineigentum oder Staatseigentum. Das Themenfeld ist komplex, mehr dazu [hier](https://www.danieladahn.de/staatseigentum-ist-privateigentum/) und [hier](https://de.wikipedia.org/wiki/Recht_der_%C3%B6ffentlichen_Sachen).

#### Was ist Entmietung?

Unter dem Begriff der Entmietung fallen Strategien, die Mieter:innen dazu bringen sollen, auszuziehen. Die Durchführung von Baumaßnahmen im oder am Haus, Lärm, Dreck, unterbrochene Wasser- oder Stromzufuhr, Verwahrlosung des Hauses gehören ebenso zur gängigen Praxis wie der Teil-Leerstand. Diese Strategien dienen häufig dazu, Luxusmodernisierungen, Neuvermietungen oder den Verkauf leerer Wohnungen oder Häuser zu ermöglichen. Auch Eigenbedarfskündigungen haben das Ziel der Entmietung und werden von manchen Initiativen daher „Entmietungswunsch“ genannt, weiter dazu [hier](https://mg-berlin.org/eigenbedarf/)

#### Was bedeutet Kostenmiete?

Unter dem mietrechtlichen Begriff der Kostenmiete ist die Miete zu verstehen, die zur Deckung der laufenden Aufwendungen für das Grundstück erforderlich ist ([§ 8 WonBindG](https://www.gesetze-im-internet.de/wobindg/__8.html)). Die Kostenmiete oder Einzelmiete umfasst die Kapitalkosten und die Bewirtschaftungskosten, die sich aus Verwaltungskosten, Instandhaltungskosten, Mietausfallwagnis und Abschreibungen zusammensetzen.  
Die Berechnung der jeweiligen Kostenmiete zeigt das Verhältnis zum Renditeanteil der Gesamtmiete, also den reinen Gewinn des Vermieters oder der Vermieterin.

Im sozialen Wohnungsbau werden die Mieten in der Regel durch eine Wirtschaftlichkeitsberechnung ermittelt. Vermieter:innen von Sozialwohnungen (= Wohnungen, die mit öffentlichen Mitteln oder zinsgünstigen Darlehen gefördert werden) dürfen von den Mieter:innen für einen bestimmten Zeitraum nur die Kostenmiete verlangen. Bei einigen Förderprogrammen erhalten Eigentümer:innen die Differenz bis zur Höhe des Mietpreises nach Mietspiegel aus öffentlichen Geldern erstattet. Nicht selten wird bei Auslaufen eines Förderprogramms mit dem Ende der [Mietpreisbindung](https://de.wikipedia.org/wiki/Mietpreisbindung) auch die Kostenmiete seitens der Eigentümer:innen sprunghaft höher berechnet.  
Die Kostenmiete sollte deutlich unter den auf dem freien Wohnungsmarkt erzielbaren Mieten liegen.  
Die laufenden Aufwendungen, also der Betrag, der durch die Kostenmiete gedeckt werden soll, werden für jedes Förderobjekt individuell ermittelt. Zur Berechnung der Einzelmiete werden die gesamten laufenden Kosten des Objekts durch die vermietbare Wohnfläche in Quadratmetern geteilt. Leider werden von Eigentümer:innen in der Praxis regelmäßig auch Kreditkosten etc. verrechnet, und in der Folge Kostenmieten genehmigt, die deutlich höher ausfallen. Mehr Information [hier](https://www.deutschlandfunk.de/berlin-wedding-kostenmiete-als-renditegarantie-trotzdem-100.html#:~:text=Die%20liegt%20in%20Berlin%20bei,in%20der%20Koloniestra%C3%9Fe%20aufgerufen%20)

#### Was haben Legionellen mit Leerstand zu tun?

Legionellen gehören zu den Risiken für Wohnende in Gebäuden mit Teil-Leerstand. Steht Wasser zu lange in den Leitungen oder wird nicht richtig erhitzt, können sich Keime wie Legionellen darin leicht vermehren. Legionellen stellen je nach Belastungsgrad insbesondere für immunschwache Personen ein Gesundheitsrisiko dar. Bei höherer Legionellenkontamination kann die Gesundheit aller Bewohner:innen gefährdet sein.

#### Was ist die Stromzählermethode?

Die „Zählermethode“ bezeichnet die Auswertung von Ver- und Entsorgungsdaten wie Strom, Müll, Wasser, Gas in Kombination mit der Gebäude-/ Einwohnerstatistik, um detaillierte Leerstandsanalysen zu erhalten. Dabei werden abgemeldete Zähler und Zähler, die nur einen sehr geringen Verbrauch anzeigen, identifiziert.  
Seit Anfang der 2000er Jahre wird die Methode von kommunaler Seite angewandt, zum Beispiel in Dortmund, Leverkusen, Schwerin oder Hannover. Seit 2009 werden in Duisburg mehr als 90 Prozent aller Stromzähler erfasst. In Berlin wurde die Methode von 2003-2010 praktiziert und konnte nach EDV-Umstellung beim Stromanbieter nicht nicht fortgeführt werden.
2022 wurde für den Bezirk Friedrichshain-Kreuzberg die [Wiedereinführung beschlossen](https://www.berlin.de/ba-friedrichshain-kreuzberg/politik-und-verwaltung/bezirksverordnetenversammlung/online/vo020.asp?VOLFDNR=10488).  
Bundesweit greifen immer mehr Kommunen auf dieses Instrument zurück.  
Die Methode ist umstritten.  
Befürworter:innen betonen, dass die Stromzählermethode nachweislich eine regelmäßige und detaillierte örtliche Leerstandserhebung ermöglicht und mit geringem Aufwand flächendeckend ohne zeitintensive Recherchearbeiten und unter Wahrung des Datenschutzes anwendbar ist.  
Kritiker:innen ist die Methode zu aufwendig und der Datenschutz schwer zu gewährleisten; außerdem wird bemängelt, dass die Daten überregional kaum vergleichbar sind.
Mehr Information dazu [hier](https://www.dielinke-muenchen-stadtrat.de/detail/leerstand-bekaempfen-ii-mit-stromzaehlermethode-leerstand-systematisch-erfassen/)  
Weitere Informationen im [Mieterecho, 2007](https://www.bmgev.de/mieterecho/325/06-leerstand-jo.html),  
Quelle [IZ Media S.19](https://cdn.iz.de/media/report/reading-rehearsal/dl-wohnungsleerstaende.pdf)

#### Was ist eine Fluktuationsreserve?

Laut [„Hassescher Regel“](https://sciendo.com/article/10.1007/s13147-015-0361-8) (einer „Daumenregel seit dem 19. Jahrhundert) benötigt ein „funktionierender“ Wohnungsmarkt leerstehende Wohnungen, um Umzüge, Instandhaltungs- und Modernisierungsmaßnahmen zu gewährleisten. Üblicherweise wird für die Fluktuationsreserve eine Leerstandsquote zwischen 1 und 3 Prozent angesetzt.
