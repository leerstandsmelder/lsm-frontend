import api from "@/utils/api.js";
import { usePlaceStore } from "@/stores/place";

export default {
  getApiImageUrl(containerId, fileName) {
    return (
      import.meta.env.VITE_API_URL +
      "/api/Attachments/" +
      containerId +
      "/download/" +
      fileName
    );
  },

  get(id, entity = "Place") {
    let url =
      entity == "Comment" ? `annotations/${id}/images` : `places/${id}/images`;
    return api.get(url);
  },
  update(image) {
    console.log("repo update image", image);
    const placeStore = usePlaceStore();
    let place = placeStore.place;
    let imageId = image.id;
    let url = `places/${place.id}/images/${imageId}`;

    return api.put(`${url}`, image);
  },

  upload(params, onUploadProgress) {
    const placeStore = usePlaceStore();
    let place = placeStore.place;
    let place_id = params["place_id"] || place.id;
    let formData = new FormData();

    Object.entries(params).forEach(([key, value]) =>
      formData.append(key, value)
    );

    const config = {
      onUploadProgress: onUploadProgress,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    };

    return api.post(`places/${place_id}/images`, formData, config);
  },
  delete(image) {
    const placeStore = usePlaceStore();
    let place = placeStore.place;
    let imageId = image.id;
    let url = `places/${place.id}/images/${imageId}`;
    return api.delete(`${url}`);
  },
};
