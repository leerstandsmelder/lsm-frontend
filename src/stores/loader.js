// Pinia Store
import { defineStore, acceptHMRUpdate } from "pinia";

export const useLoaderStore = defineStore({
  id: "loader",
  state: () => ({
    loading: false,
  }),
  actions: {
    show() {
      this.loading = true;
    },
    hide() {
      setTimeout(() => {
        this.loading = false;
      }, 1000);
    },
  },
  persist: false,
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useLoaderStore, import.meta.hot));
}
