# Leerstandsmelder VueJS SPA Frontend

Progressiv web app based on Vue3, Vuetify, maplibre-gl

![image info](resources/screens/screen_region.png)


## Installation

### Requirements

* Node 16+
* yarn or npm

### Project setup

```
yarn install
```
Basic project parameters can found in .env.example or can be set as enviroments variables.

```
cp .env.example .env
```

It requires API tokens for [Geoapify](https://www.geoapify.com) (Geocoding) and [Maptile](https://www.maptiler.com/) (maps):


### Compiles and hot-reloads for development

```
yarn dev
```

### Compiles and minifies for production

```
yarn build 
yarn build:production

```

### Run your unit tests

```
npm run test:unit
```

### Run cypress tests

```
yarn e2e
yarn e2e:record
yarn cy:open
```

### Lints and fixes files

```
yarn lint
yarn lint:fix

```

### Prettier

```
yarn format

```

### Customize configuration

The build system is [vite](https://vitejs.dev)

The configuration can be found in [vite.config.js](vite.config.js)

Vuetify layout basics are configured in [settings.scss](src/styles/settings.scss)

### Create dockerized container

Command will create a ngix based image

```
docker build -t registry.gitlab.com/leerstandsmelder/lsm-frontend:development .
```

### Run dockerized container

```
docker run -p 8080:8080 registry.gitlab.com/leerstandsmelder/lsm-frontend:development
```
