import { ref, onMounted, onUnmounted } from "vue";

export default function useDetectColorScheme() {
  const colorSchemes = {
    lsmDarkTheme: "(prefers-color-scheme: dark)",
    lsmLightTheme: "(prefers-color-scheme: light)",
  };
  const colorKeys = Object.keys(colorSchemes);
  const scheme = ref("lsmLightTheme");

  // If we don't have a matchMedia key just return the scheme as is
  if (!window.matchMedia) {
    return scheme;
  }

  // A simple listener for our color scheme changes
  function listener(e) {
    console.log("listen", e);
    if (!e || !e.matches) {
      return scheme;
    }

    for (const sn of colorKeys) {
      console.log("keys", sn, e.media);
      if (e.media === colorSchemes[sn]) {
        scheme.value = sn;
        break;
      }
    }
  }

  // Add our listener for all themes
  onMounted(() => {
    console.log("mounted", colorKeys);
    colorKeys.forEach((sn) => {
      const mq = window.matchMedia(colorSchemes[sn]);
      mq.addEventListener("change", listener);

      listener(mq);
    });
  });

  // Remove listeners, no memory leaks
  onUnmounted(() => {
    colorKeys.forEach((sn) => {
      const mq = window.matchMedia(colorSchemes[sn]);
      mq.removeEventListener("change", listener);
    });
  });

  return scheme;
}
