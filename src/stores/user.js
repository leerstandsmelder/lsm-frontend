// Pinia Store
import { defineStore, acceptHMRUpdate } from "pinia";
import { RepositoryFactory } from "@/utils/repositories/repositoryFactory";

export const useUserStore = defineStore({
  id: "user",
  state: () => ({
    reset: false,
    reset_token: "",
    user: {
      nickname: "",
      email: "",
      message_me: false,
      notify: false,
      share_email: false,
      accept_terms: false,
    },
    places: [],
    comments: [],
  }),
  getters: {
    getUser: (state) => state.user,
    myComments: (state) => state.comments,
    myPlaces: (state) => state.places,
  },
  actions: {
    fetchUser() {
      const UserRepository = RepositoryFactory.get("users");
      return UserRepository.get().then((data) => {
        this.user = data.data.attributes;
        return data;
      });
    },
    setUser(data) {
      this.user = data.user;
      return data.user;
    },
    updateUser() {
      const UserRepository = RepositoryFactory.get("users");
      return UserRepository.update(this.user.id, this.user);
    },
    reset() {
      this.$reset();
    },
    newPassword(email) {
      const UserRepository = RepositoryFactory.get("users");
      return UserRepository.newPassword(email);
    },
    updatePassword(password) {
      const UserRepository = RepositoryFactory.get("users");
      return UserRepository.updatePassword(password);
    },
    validatePasswordToken(token) {
      const UserRepository = RepositoryFactory.get("users");
      return UserRepository.validatePasswordToken(token);
    },
    fetchPlaces() {
      const UserRepository = RepositoryFactory.get("users");
      return UserRepository.getPlaces(this.user.id, {
        include_region: true,
      }).then((data) => {
        this.places = data;
        return data;
      });
    },
    fetchComments() {
      const UserRepository = RepositoryFactory.get("users");
      return UserRepository.getComments(this.user.id).then((data) => {
        this.comments = data;
        return data;
      });
    },
    clear() {
      this.$reset();
    },
    destroy() {
      const UserRepository = RepositoryFactory.get("users");
      return UserRepository.destroy(this.user.id);
    },
  },
  persist: true,
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useUserStore, import.meta.hot));
}
