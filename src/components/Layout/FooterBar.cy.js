import FooterBar from "./FooterBar.vue";

describe("<FooterBar />", () => {
  it("renders footer links and copyright", () => {
    cy.mount(FooterBar);
    cy.get("#copyright strong").should(
      "have.text",
      "© 2024 Leerstandsmelder.de"
    );
    cy.get(".footer-links a").should("have.length", 3);
    cy.get(".footer-links a:first").should("have.text", "Impressum");
  });
});
