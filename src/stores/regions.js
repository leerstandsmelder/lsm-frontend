// Pinia Store
import { defineStore, acceptHMRUpdate } from "pinia";
import { RepositoryFactory } from "@/utils/repositories/repositoryFactory";

export const useRegionsStore = defineStore({
  id: "regions",
  state: () => ({
    status: "initial",
    regions: {},
  }),
  getters: {
    regionsStatus: (state) => state.status,
    getRegions: (state) => state.regions,
  },
  actions: {
    load() {
      if (this.status == "initial" || this.status == undefined) {
        this.fetchRegions();
      }
    },
    fetchRegions(data) {
      this.status = "loading";
      const MapsRepository = RepositoryFactory.get("maps");
      return MapsRepository.getAllMaps().then((resp) => {
        this.regions = resp;
        this.status = "loaded";
        this.regions = resp;
        return resp;
      });
    },

    clear() {
      //TODO: reset locations
      this.$reset();
    },
  },
  persist: false,
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useRegionsStore, import.meta.hot));
}
