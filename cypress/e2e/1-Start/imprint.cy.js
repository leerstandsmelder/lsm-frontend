/// <reference types="cypress" />

// Welcome to Cypress!
//
// This spec file contains a variety of sample tests
// for a todo list app that are designed to demonstrate
// the power of writing tests in Cypress.
//
// To learn more about how Cypress works and
// what makes it such an awesome testing tool,
// please read our getting started guide:
// https://on.cypress.io/introduction-to-cypress

describe("Imprint screen", () => {
  beforeEach(() => {
    cy.intercept_all_apis();
    cy.visit("/imprint");
  });

  it("loads and displays imprint", () => {
    cy.get(".imprint-view", { timeout: 10000 }).should(
      "contain",
      "Verein Gängeviertel e.V."
    );
  });
});
