## Kontakt

### Allgemein Kontaktanfragen

- Für allgemeine Fragen wendet euch an hallo@leerstandsmelder.de
- Bei technischen Problemen kann euch manchmal geholfen werden: technik@leerstandsmelder.de
- Bei Interesse Leerstandsmelder.de in deiner Stadt zu starten: neue-stadt@leerstandsmelder.de


#### Verantwortlich für www.leerstandsmelder.de ist

<pre>
Verein Gängeviertel e.V.,
vertreten durch den Vorstand
Vereinsregisternummer 20574 (Amtsgericht Hamburg)

Valentinskamp 28a
20355 Hamburg
Telefon: 040 - 530 22 699
</pre>





### E-Mail Kontakte Stadtinitativen

Augsburg: Augsburg [at] leerstandsmelder.de

Bad Oldesloe: info [at] oase-oldesloe.de

Berlin: berlin [at] leerstandsmelder.de

Bochum: mensch.mieter [at] mvbo.de

Bonn: bonn [at] leerstandsmelder.de

Braunschweig: braunschweig [at] leerstandsmelder.de

Bremen: bremen [at] leerstandsmelder.de

Chemnitz: chemnitz [at] leerstandsmelder.de

Dortmund: dortmund [at] leerstandsmelder.de

Erlangen: erlangen [at] leerstandsmelder.de

Essen: essen [at] leerstandsmelder.de

Frankfurt: frankfurt [at] leerstandsmelder.de

Gießen: giessen [at] leerstandsmelder.de

Görlitz: goerlitz [at] leerstandsmelder.de

Göttingen: goettingen [at] leerstandsmelder.de

Hamburg: hamburg [at] leerstandsmelder.de

Heidelberg: Heidelberg [at] leerstandsmelder.de

Kaiserslautern: kaiserslautern [at] leerstandsmelder.de

Innsbruck: buergerinitiativen.innsbruck [at] gmail.com

Kassel: kassel [at] leerstandsmelder.de

Ludwigsburg: elisabeth.riether [at] googlemail.com

Luxemburg: luxemburg [at] leerstandsmelder.de

Münster: muenster [at] leerstandsmelder.de

Meiningen: meiningen [at] leerstandsmelder.de

Nürnberg: nuernberg [at] leerstandsmelder.de

Oberhausen: oberhausen [at] leerstandsmelder.de

Osnabrück: osnabrueck [at] leerstandsmelder.de

RegionBasel: regionbasel [at] leerstandsmelder.de

Rostock: rostock [at] leerstandsmelder.de

Salzburg: salzburg [at] leerstandsmelder.de

Schwerin: schwerin [at] leerstandsmelder.de

Stuttgart: stuttgart [at] leerstandsmelder.de

Tübingen-Reutlingen: tuereu [at] leerstandsmelder.de

Weimar: weimar [at] leerstandsmelder.de

Wien: wien [at] leerstandsmelder.de

Wuppertal: wuppertal [at] leerstandsmelder.de


