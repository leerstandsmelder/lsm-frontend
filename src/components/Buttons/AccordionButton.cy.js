import AccordionButton from "./AccordionButton.vue";

describe("AccordionButton", () => {
  it("should mount with props", () => {
    cy.mount(AccordionButton, {
      props: {
        caption: "MyCaption",
        size: "xl",
        icon: "$mdiClose",
        isOpen: true,
      },
      slots: {
        default: "Hello there!",
      },
    });
    cy.get("button.v-expansion-panel-title").contains("MyCaption");
    cy.get("div.v-expansion-panel-text__wrapper").should(
      "have.text",
      "Hello there!"
    );
    cy.get("div.v-expansion-panels").should("have.class", "xl");
  });

  it("when button is clicked, Accordion slot text should be visible", () => {
    cy.mount(AccordionButton, {
      props: {
        caption: "Slot open",
      },
      slots: {
        default: "Hello i am open!",
      },
    });

    cy.get("button").click();
    cy.get("div.v-expansion-panel-text__wrapper").should(
      "have.text",
      "Hello i am open!"
    );
  });
});
