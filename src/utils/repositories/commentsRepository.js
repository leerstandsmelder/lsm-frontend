import api from "@/utils/api.js";
import { usePlaceStore } from "@/stores/place";

export default {
  query(type, params) {
    return api.query("annotations" + (type === "filter" ? "/filter" : ""), {
      params: params,
    });
  },
  fetch(placeId = "") {
    const placeStore = usePlaceStore();
    let place = placeStore.place;
    let url = `places/${place.id}/annotations`;
    return api.get(url);
  },
  get(commentId) {
    const placeStore = usePlaceStore();
    let place = placeStore.place;
    let url = `places/${place.id}/annotations/${commentId}`;
    return api.get(url);
  },
  create(params) {
    const placeStore = usePlaceStore();
    let place = placeStore.place;
    params["place_id"] = place.id;
    if (params["status"]) {
      params["status_id"] = params["status"];
    }
    let url = `places/${place.id}/annotations/`;
    return api.post(url, { annotation: params });
  },
  update(params) {
    const placeStore = usePlaceStore();
    let place = placeStore.place;
    let slug = params.id;
    let url = `places/${place.id}/annotations/${slug}`;
    return api.put(url, { annotation: params });
  },
  destroy(slug) {
    const placeStore = usePlaceStore();
    let place = placeStore.place;
    let url = `places/${place.id}/annotations/${slug}`;
    return api.delete(url);
  },
};
