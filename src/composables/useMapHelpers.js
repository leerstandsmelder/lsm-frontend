import { ref } from "vue";
import { useSiteStore } from "@/stores/site";
const siteStore = useSiteStore();

import { Marker, Popup } from "maplibre-gl";

export function useMapHelpers() {
  const geoapify_key =
    import.meta.env.VITE_GEOAPIFY_KEY || "xxx_geoapify_key_missing_xxx";
  const maptiler_key =
    import.meta.env.VITE_MAPTILER_KEY || "xxx_maptiler_key_missing_xxx";
  const maptiler_map_light =
    import.meta.env.VITE_MAPTILER_MAP_LIGHT ||
    "xxx_maptiler_map_light_missing_xxx";
  const maptiler_map_dark =
    import.meta.env.VITE_MAPTILER_MAP_DARK ||
    "xxx_maptiler_map_dark_missing_xxx";

  function mapStyleLink() {
    let theme = siteStore.getThemeColor;
    let maptiler_map = "";
    if (theme == "lsmDarkTheme") {
      maptiler_map = maptiler_map_dark;
    } else {
      maptiler_map = maptiler_map_light;
    }
    return (
      "https://api.maptiler.com/maps/" +
      maptiler_map +
      "/style.json?key=" +
      maptiler_key
    );
  }

  function updateGeoJson(features) {
    let collection = [];
    collection = features.map((x, index) => ({
      type: "Feature",
      geometry: x.geometry,
      properties: x.properties,
      id: index + 1,
    }));
    return {
      data: ref({
        type: "FeatureCollection",
        features: collection,
      }),
    };
  }

  function createMarker(dataElement, map, clickCallback, selectedID = "") {
    let el = document.createElement("div");

    el.className = "marker";
    if (selectedID == dataElement.properties.id) {
      el.className += " activeMarker";
    }

    if (dataElement.properties.status) {
      el.className += " std_" + dataElement.properties.status;
    }
    // if (dataElement.properties.thumb_url) {
    //   el.style.backgroundImage = "url(" + dataElement.properties.thumb_url + ")";
    // }
    el.setAttribute("data-title", dataElement.properties.title);

    // el.style.width = "30px";
    // el.style.height = "30px";

    el.addEventListener("click", function () {
      clickCallback(dataElement);
    });

    // add marker to map
    let mark = new Marker(el)
      .setLngLat(dataElement.geometry.coordinates)
      .addTo(map);

    return mark;
  }

  function addMapPointerEvents(map, source) {
    map.on("mouseenter", source, function () {
      map.getCanvas().style.cursor = "pointer";
    });
    map.on("mouseleave", source, function () {
      map.getCanvas().style.cursor = "";
    });
  }

  function addMapClickEvent(map, source, callback) {
    map.on("click", source, function (e) {
      let element = e.features[0];
      let coordinates = element.geometry.coordinates.slice();
      let mag = element.properties.title; //  + " (" + element.properties.id + ")"

      while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
        coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
      }

      callback(element);

      new Popup()
        .setLngLat(coordinates)
        .setHTML("" + mag + "<br>")
        .addTo(map);
    });
  }

  function addMapClusterClickEvent(
    map,
    source,
    cluster_source,
    callback = () => {}
  ) {
    map.on("click", source, async function (e) {
      let features = map.queryRenderedFeatures(e.point, {
        layers: [source],
      });
      let clusterId = features[0].properties.cluster_id;
      const zoom = await map
        .getSource(cluster_source)
        .getClusterExpansionZoom(clusterId);
      map.easeTo({
        center: features[0].geometry.coordinates,
        zoom: zoom,
      });
    });
  }

  const reverseGeocode = async function (config) {
    const features = [];
    try {
      // let request =
      //   "https://nominatim.openstreetmap.org/search?q=" +
      //   config.query +
      //   "&format=geojson&polygon_geojson=1&addressdetails=1";
      //https://api.geoapify.com/v1/geocode/reverse?lat=52.547723593697555&lon=13.427401809691332&format=json&apiKey=YOUR_API_KEY
      let request =
        "https://api.geoapify.com/v1/geocode/reverse?lat=" +
        config.lat +
        "&lon=" +
        config.lon +
        "&lang=" +
        siteStore.language +
        "&format=json&apiKey=" +
        geoapify_key;

      const response = await fetch(request);
      const geojson = await response.json();
      console.log("reverse response", geojson);
      for (let feature of geojson.results) {
        let center = [0, 0];
        if (feature.bbox) {
          center = [
            feature.bbox[0] + (feature.bbox[2] - feature.bbox[0]) / 2,
            feature.bbox[1] + (feature.bbox[3] - feature.bbox[1]) / 2,
          ];
        } else {
          center = [feature.lon, feature.lat];
        }

        let point = {
          type: "Feature",
          geometry: {
            type: "Point",
            coordinates: center,
          },
          //place_name: feature.properties.display_name,
          place_name: feature.formatted,
          //properties: feature.properties,
          text: feature.formatted,
          place_type: feature.result_type || ["place"],
          center: center,
        };
        let merged = { ...point, ...feature };
        features.push(merged);
      }
    } catch (e) {
      console.error(`Failed to reverseGeocode with error: ${e}`);
    }

    return {
      features: features,
    };
  };

  const forwardGeocode = async function (config) {
    const features = [];
    try {
      const request = `https://nominatim.openstreetmap.org/search?q=${config.query}&format=geojson&polygon_geojson=1&addressdetails=1`;
      const response = await fetch(request);
      const geojson = await response.json();
      for (let feature of geojson.features) {
        let center = [0, 0];
        if (feature.bbox) {
          center = [
            feature.bbox[0] + (feature.bbox[2] - feature.bbox[0]) / 2,
            feature.bbox[1] + (feature.bbox[3] - feature.bbox[1]) / 2,
          ];
        } else {
          center = [feature.lon, feature.lat];
        }

        let point = {
          type: "Feature",
          geometry: {
            type: "Point",
            coordinates: center,
          },
          place_name: feature.properties.display_name,
          properties: feature.properties,
          text: feature.properties.display_name,
          place_type: ["place"],
          center,
        };
        let merged = { ...point, ...feature };
        features.push(merged);
      }
    } catch (e) {
      console.error(`Failed to forwardGeocode with error: ${e}`);
    }

    return {
      features: features,
    };
  };

  function mapSetData(map, source, point) {
    map.getSource(source).setData({
      type: "FeatureCollection",
      features: [
        {
          type: "Feature",
          properties: { name: point.place_name },
          geometry: {
            type: "Point",
            coordinates: [point.lon, point.lat],
          },
        },
      ],
    });
  }

  function roundToX(num, decimals) {
    return +(Math.round(num + "e" + decimals) + "e-" + decimals);
  }

  return {
    geoapify_key,
    maptiler_key,
    maptiler_map_light,
    maptiler_map_dark,
    mapStyleLink,
    updateGeoJson,
    createMarker,
    addMapPointerEvents,
    addMapClusterClickEvent,
    addMapClickEvent,
    reverseGeocode,
    forwardGeocode,
    mapSetData,
    roundToX,
  };
}
