## HILFE + SUPPORT

Die Inhalte stellen keine Rechtsberatung dar, es handelt sich lediglich um eine allgemeine Darstellung der Rechtslage, die nicht auf Einzelfälle eingeht.

Für einfacheren barrierefreien Zugang wird auf Leerstandsmelder die Gender-Form mit Doppelpunkt verwendet.
Diese Personenbezeichnungen beziehen sich aber ausdrücklich– sofern nicht anders kenntlich gemacht – auf alle existierenden Identitäten.

### FAQ

#### Warum müsst Ihr Euch registrieren, um Leerstand zu melden?

Die Registrierung sichert die Qualität der Einträge und regelt die Rechte und Pflichten von Nutzer:innen und der Plattform. weitere Info in unseren [AGB](/site/terms_de)

#### Ich habe mein Passwort vergessen, was nun?

Auf der Anmeldeseite gibt es die Funktion _Passwort vergessen_, dort könnt Ihr im nächsten Schritt Eure E-Mail-Adresse eingeben und ein neues Passwort anfordern. Mit dem per E-Mail erhaltenen Passwort könnt Ihr Euch einloggen und im Profilbereich ein neues Wunschpasswort erstellen.

#### Sind personenbezogene Daten bei Leerstandsmelder sicher?

Nutzer:innen bleiben anonym, wenn sie nicht ihren Klarnamen verwenden. Die Einträge fallen unter die Meinungsfreiheit im Internet (Hinweis: Keine Beleidigungen etc., bitte sachlich bleiben) und die Leerstandsmelder-Betreiber:innen geben die personenbezogenen Daten der Nutzer:innen nicht an Dritte weiter. Die Plattform verwendet weder _Google Maps_ noch _Facebook_ Like-Buttons oder andere Dienste, die das Nutzerverhalten analysieren. Das Kartenmaterial stammt von _OpenStreetMap_. Die einzigen Cookies, die verwendet werden, dienen der Authentifizierung der angemeldeten Nutzer:innen.

#### Wozu Leerstände sammeln?

Ein umfassender und aktueller Überblick über das Ausmaß von Leerstand kann ein wirksames Druckmittel gegenüber der Stadtentwicklungs- und Regionalpolitik sowie den Eigentümer:innen bei Leerstand, das heißt bei Zweckentfremdung, sein. Siehe [Unsere Forderungen](/site/claim/general_de)

#### Welche Arten von Leerstand zeigt Leerstandsmelder?

Leerstandsmelder zeigt alle gemeldeten Leerstände. Dazu gehören nicht nur marktaktive Leerstände, sondern auch _Problemimmobilien_ oder brachliegende Baustellen. Über die Plattform können Leerstände von einzelnen Wohnungen, ganzen Gebäuden, Gewerbeflächen oder Baudenkmälern an vielen Standorten erfasst werden. Besonderes Augenmerk liegt dabei auf strukturellen und spekulativen Leerständen. Mehr Info bei [Was ist Leerstand](/site/definition/definition_de)

#### Wie erkenne ich Leerstand?

Generell empfiehlt es sich, zunächst Informationen zu sammeln, zu recherchieren und zu Dokumentationszwecken auch Fotos zu machen. Ab welchem Wissensstand ein Leerstand gemeldet wird, liegt bei den Nutzer:innen. Wichtig ist, dass Beobachtungen und Zweifel mit der Community geteilt und Vermutungen klar benannt werden. Mehr Info bei [Wie erkenne ich Leerstand](/site/definition/spot_de) und [Was ist Leerstand](/site/definition/definition_de)

#### Warum sind sorgfältige Angaben in einem Eintrag so wichtig?

Je genauer und zuverlässiger die Einträge sind, desto bessere Daten stehen zur Verfügung. Ziel ist es, Wissen zu teilen. Um Fehlinformationen zu vermeiden, wurden leicht verständliche Kategorien für die Leerstandserfassung eingerichtet. Den Nutzer:innen steht es offen, zusätzliche Informationen in das Kommentarfeld einzutragen. Auch Vermutungen und Hörensagen sollten hier unbedingt kenntlich gemacht werden.

#### Helfen Einträge bei Leerstandsmelder Spekulant:innen bei der Verwertung von Immobilien?

Einige Menschen befürchten, dass die Einträge Spekulant:innen die Arbeit erleichtern könnten. Da Leerstandsmelder vom Grundsatz her eine Plattform zur [Transparenz](/site/support/goodtoknow_de) für alle ist, können wir nicht ausschließen, dass auch Immobilienverwerter:innen die Seite besuchen. Es ist jedoch zu bezweifeln, dass Leerstandsmelder der Gentrifizierung zuarbeitet, da Spekulant:innen, Makler:innen und Investor:innen bereits exklusive Netzwerke, „Immobilienstammtische“ und Kontakte zur Politik aufgebaut haben und sich außerdem eher anderweitig über Leerstände informieren.  
Leerstandsmelder wirkt demgegenüber als Korrektiv und fördert die Informationsgleichheit, zeigt die Perspektive der Zivilgesellschaft und damit auch das, was die Verwerter nicht zeigen wollen. Die Diskussion um Leerstand schafft zudem eine Protestkultur, die sich gegen Projektentwicklungen und kapitalstarke Investoren richtet. Leerstandsmelder fördert Informationsgleichheit und Transparenz.  
Was die Veröffentlichung eines Eintrages auf Leerstandsmelder bewirkt oder bewirken könnte, ist individuell zu bewerten.

Für die beteiligten Großstädte lässt sich pauschal sagen, dass es sich bei den meisten gemeldeten Leerständen um Immobilien handelt, die bereits aus dem Markt gefallen sind. Vielfach stehen sie leer, weil Investitionen keine Rendite erwarten lassen oder weil sie bereits Spekulationsobjekte sind.
In strukturschwachen Regionen nimmt auch die gezielte Spekulation zu. Inwieweit in einzelnen Regionen ein „Anschub“ von Investitionen gewünscht oder notwendig ist, ist regional unterschiedlich.

#### Kann ich auf Leerstandsmelder eine neue Wohnung finden?

Leerstandsmelder ist keine Wohnungsbörse. In der Vergangenheit haben jedoch bereits einige Personen über die Plattform eine neue Wohnung gefunden.  
Die meisten Objekte sind jedoch bei uns gelistet, weil sie (absichtlich) dem Markt entzogen werden; oft haben die Eigentümer:innen dieser Flächen aktuell keine Vermietungsabsicht.

#### Werden Leerstände und Eigentümer:innen hier angeprangert?

Wenn Flächen oder Räume einer sinnvollen Nutzung entzogen werden, leerstehen oder verfallen, kann und sollte das benannt werden. Eigentümer:innen können sich ebenfalls beteiligen und beim betreffenden Eintrag Hintergründe für den Leerstand hinzufügen.

#### Darf der Name von Eigentümer:innen einer Immobilie auf Leerstandsmelder veröffentlicht werden?

Handelt es sich um eine [juristische Person](/site/support/faq_de), kann der Name veröffentlicht werden. Der Klarname einer [natürlichen Person](/site/support/faq_de) darf aus Datenschutzgründen nicht veröffentlicht werden.

#### Was macht Leerstandsmelder mit den gesammelten Einträgen?

Informationen zu Leerständen werden in unserer Datenbank erfasst, durch die Optionen im Meldemenü kategorisiert und mittels Filteroptionen auf der [Karte](/region) für alle dargestellt.
Andere Nutzer:innen können im Anschluss weitere Informationen hinzufügen, so wird Leerstand sichtbar dokumentiert.  
 Perspektivisch arbeiten wir an einer besseren statistischen Auswertung und suchen dafür Kooperationen mit Projekten und [Partner:innen](/site/about/partner_de), die sich für eine nachhaltige Stadt- und Regionalentwicklung engagieren.  
Immer wieder bekommen wir Anfragen von Wissenschaftler:innen, Studierenden, Journalist:innen oder stadtpolitisch Aktiven, die lokale oder überregionale Daten erfragen. Über mögliche Zusammenarbeit wird in basisdemokratischen Prozessen beim Leerstandsmelder-Meeting entschieden.

Wir freuen uns, mit gemeinwohlorientierten, sozialen, klimagerechten, nachhaltigen, verantwortungsbewussten Projekten und Vorhaben zu kooperieren!

#### Wie zuverlässig sind die Daten von Leerstandsmelder und wie lassen sie sich interpretieren?

Je mehr Menschen sich beteiligen, desto zuverlässiger werden die Daten.
Der direkte Eintrag von Objekten durch die Nutzer:innen ist zugleich Stärke und Schwäche von Leerstandsmelder. Nicht alle Einträge können durch die lokalen Administrator:innen validiert werden.
Meldungen werden zwar auf Plausibilität, nicht aber alle Daten auf ihre Validität und Aktualität geprüft. Die Community der Nutzer:innen trägt dazu bei, fehlerhafte Einträge zu minimieren. Trotz dieses Mehraugenprinzips ist die Aussagekraft der Daten begrenzt. Leerstandsmelder erhebt nicht den Anspruch, die Leerstandssituation einer Region vollständig abzubilden. Auch bei der Interpretation der Daten ist Vorsicht geboten: Eine niedrige Leerstandsdichte auf der Leerstandsmelderkarte bedeutet nicht zwangsläufig, dass auch der tatsächliche Leerstand besonders niedrig ist. Die Dichte der Einträge spiegelt in erster Linie die Aktivität und die Wege der Nutzer:innen wider.

#### Warum wird mein Eintrag nicht sofort angezeigt?

In einigen Städten werden Eure Einträge geprüft, bevor sie in die Datenbank aufgenommen und online gestellt werden. Bitte habt etwas Geduld mit den Administrator:innen.

#### In Eurer Region gibt es noch keinen Leerstandsmelder – lässt sich der Leerstand dennoch hier melden?

Leider nein. Das Verfahren bedarf einer lokalen Betreuung.

#### Wie geht es nach der Meldung eines Leerstands weiter?

Nach einer kurzen Prüfung wird der Eintrag Teil der Datenbank, erscheint auf der [Karte](/region) und kann auch von anderen Nutzer:innen fortgeschrieben werden. Wir arbeiten an der statistischen Auswertung, um damit einen Beitrag zur Dokumentation der Entwicklung von Stadt und Region zu leisten.

#### Wie können Informationen in einem vorhandenen Beitrag ergänzt, aktualisiert oder berichtigt werden?

Veränderungen, Neuigkeiten oder weiterführende Informationen können von allen angemeldeten Nutzer:innen zu einem bestehenden Eintrag hinzugefügt werden. Dadurch entsteht eine kleine Historie zu der jeweiligen Adresse. Die ursprünglichen Einträge können nur notfalls von den Administrator:innen korrigiert werden. Bei Schwierigkeiten wendet euch am besten per E-Mail an die Administrator:innen eurer Stadt oder Region. Die Kontakte findet ihr [hier](/site/about/network_de)

#### Welche Möglichkeiten gibt es, um sich an Leerstandsmelder zu beteiligen?

Neben der Leerstandsmeldung können Leerstandseinträge von allen angemeldeten Nutzer:innen überprüft und aktualisiert werden. Wenn Ihr Euch darüber hinaus beteiligen wollt, findet ihr weitere Informationen [hier](/site/participate/how_de)

#### Ist es sinnvoll, wiederholt auf Leerstand hinzuweisen?

Bestehende Einträge können gerne aktualisiert werden (z.B. alle 6 Monate). Auch wenn es keine neuen Informationen oder Änderungen der Leerstandssituation gibt, ist eine regelmäßige Dokumentation wertvoll. So halten wir gemeinsam die Datenbank aktuell.

#### Warum sollen Fotos hochgeladen werden?

Bilder können zusätzliche Informationen enthalten und die Entwicklung dokumentieren. Der Eintrag wird dadurch für alle interessanter und aussagekräftiger. Leerstandsmelder darf die eingestellten Bilder (ohne personenbezogene Daten) zur Auswertung, Analyse und Veröffentlichung im Sinne der Plattform nutzen und kann so einen Beitrag zur Dokumentation der Stadt- und Regionalentwicklung leisten.

#### Was ist für das Hochladen von Fotos zu beachten?

Bilder können in allen gängigen Formaten bis max. 20 MB hochgeladen werden. Es sollten nur Bilder hochgeladen werden, für die die Bildrechte vorliegen, das heißt keine Screenshots von _Google Maps_ oder ähnliches, sondern am besten ausschließlich eigene Bilder. Es dürfen keine personenbezogenen Daten (z.B. Telefonnummern, E-Mail-Adressen oder Namen) von Eigentümer:innen genannt werden, es sei denn, diese sind am Objekt selbst veröffentlicht (z.B. auf einer Bautafel, die vor dem Gebäude steht). Außerdem sollten keine Personen oder Autokennzeichen auf den Bildern zu erkennen sein.

#### Können auch Eigentümer:innen ihren Leerstand melden?

Auch Eigentümer:innen können sich beteiligen und Informationen hier teilen.

#### Warum dürfen Menschen mit ihrem Eigentum nicht machen, was sie wollen und es leerstehen lassen?

Artikel 14, Absatz 2 des Grundgesetzes besagt, dass Eigentum verpflichtet: „Sein Gebrauch soll zugleich dem Wohle der Allgemeinheit dienen.“ Weiter bei [Warum Leerstand schadet](/site/definition/harm_de)

#### Warum braucht es Transparenz auf dem Immobilienmarkt?

Von mehr Transparenz profitieren alle: Die Bewohner:innen wissen, in welchen Händen ihre Wohnung ist. Journalist:innen gelangen an Informationen, um auf Missstände hinzuweisen und die Stadt erhält einen wichtigen Überblick über die Eigentumsverhältnisse. Die Diskussion um die Gestaltung einer lebenswerten Stadt kann auf einer transparenten Grundlage zielgerichteter geführt werden. Kurz: Informationen zur Stadtentwicklung sollten für alle zugänglich sein. (siehe [Was ist das Transparenzregister](/site/support/goodtoknow_de))

Der deutsche Immobilienmarkt ist jedoch intransparent wie kaum eine andere Branche. Weder sind Informationen für die Zivilgesellschaft zugänglich, noch kennen die Kommunen den Markt. Der Inhalt des [Grundbuchs](/site/support/goodtoknow_de) ist ein gut gehütetes Geheimnis. Intransparenz begünstigt Kriminalität, undurchsichtige Firmengeflechte, verdeckte Verkäufe und Geldwäsche. Die Möglichkeit, Geld anonym anzulegen oder Gewinne an der Steuer vorbei zu verschieben, ist gängige Praxis. In einigen EU-Ländern ist es dagegen üblich, dass die Eigentumsverhältnisse öffentlich einsehbar sind.

Nach ersten Rechercheprojekten von _[CORRECTIV](https://correctiv.org)_ in Hamburg und Berlin gab es bereits Vorstöße von Oppositionsparteien im Bundestag für ein offenes Immobilienregister, in dem zumindest die Firmen als Eigentümer:innen und damit auch die tatsächlichen Eigentümer:innen einsehbar sein sollten. Einige große Eigentümer:innen setzen sich auch für mehr Transparenz ein. Die Große Koalition hat diese Vorstöße jedoch blockiert.
Mehr Informationen dazu [hier](https://correctiv.org/aktuelles/wem-gehoert-die-stadt/2020/07/15/intransparenz-geldwaesche-steuern-gesetz-oase-muenchen-immobilien-wem-gehoert/)  
Mehr zum Thema Transparenz auf dem Immobilienmarkt [hier](https://correctiv.org/themen/wem-gehoert-die-stadt/)

### Leerstandsmelder Organisation und Beteiligung

#### Wie ist Leerstandsmelder organisiert?

Ein kleines Kernteam kümmert sich um den allgemeinen Betrieb der Plattorm, ansonsten ist die Arbeit in den [beteiligten Regionen](/site/about/network_de) lokal organisiert. Eine breite Akteurskonstellation und Vielfalt sind uns wichtig. Die Ausrichtung als kollektiver Datenpool macht die Plattform unabhängig von kommunalen Informationskanälen.  
Lokale Initiativen prüfen die Richtigkeit der Einträge für ihre Stadt oder Region. Die dezentrale Organisation verteilt die Arbeit auf viele Schultern, fördert die Einbindung von lokalem Wissen und die Bekanntheit der Website vor Ort. Aus dem gleichen Grund gibt es Leerstandsmelder nicht flächendeckend oder bundesweit. Die lokalen Initiativen bringen ganz unterschiedliche Perspektiven ein. Die meisten Initiativen sind Vereine, die nachhaltige, partizipative, soziale oder kulturelle Ziele im städtischen Kontext verfolgen. Aber auch ein Planungsbüro, ein Citymanagement, eine Hochschulgruppe, ein Stadterneuerungsverein und eine Wirtschaftsförderungsgesellschaft sind oder waren Träger lokaler Leerstandsmelder. Diese Heterogenität wirkt sich positiv auf die Akzeptanz des gemeinsamen Datenpools aus.  
Leerstandsmelder ist institutionell und politisch unabhängig.

#### Wie ist Leerstandsmelder entstanden?

Das Konzept eines kollektiven Datenpools wurde Ende 2010 vom *[Gängeviertel e.V.](https://das-gaengeviertel.info/) in Hamburg entwickelt, um auf die Verschwendung von Ressourcen aufmerksam zu machen und die Debatte um das „Recht auf Stadt" voranzutreiben. Hervorgegangen ist der Verein aus der Initiative *Komm in die Gänge\*, die 2009 zwölf denkmalgeschützte, aber von Abriss bedrohte Häuser in der Hamburger Innenstadt besetzte und anschließend den Ankauf der Immobilien durch die Stadt erreichte. Bis heute sind zahlreiche weitere Städte und Regionen hinzugekommen. Mehr Info bei [Gründung und Geschichte](/site/about/history_de)

#### Wie finanziert sich Leerstandsmelder?

Das Betreiben der Plattform verursacht lokal keine Kosten, eventuell benötigte Finanzierung von Infomaterialien und Veranstaltungen wird jeweils vor Ort organisiert. Alle Serverkosten werden vom Gängeviertel e.V. getragen.  
Regional gab es im Laufe der Geschichte des Leerstandsmelder unterschiedliche [Förderpartner](/site/about/partner_de). Darunter waren Mieter:innenvereine, die _[Rosa-Luxemburg-Stiftung](https://www.rosalux.de)_, eine Crowdfunding-Kampagne 2015 und einige mehr. Die Neuentwicklung der Plattform (2022-2023) wurde durch den _[Prototype Fund](https://prototypefund.de)_ des _[BMBF](https://www.bmbf.de)_ gefördert. Mehr Info bei [Gründung und Geschichte](/site/about/history_de)

#### Warum basiert Leerstandsmelder auf Open Source Software?

Wir unterstützen den Einsatz von freier Software, um den Zugang und das Teilen von Wissen zu erleichtern und die Selbstbestimmung der Gesellschaft zu fördern.  
Die Plattform ist _Open Source_ angelegt und bietet Einblick in den [Quellcode](/site/about/opensource_de).

#### Ihr möchtet inhaltliche Beiträge mit Leerstandsmelder teilen?

Interessante Informationen und Links zum Thema Leerstand könnt Ihr jederzeit per E-Mail an <hallo(at)leerstandsmelder.de> und den lokalen Leerstandsmelder vor Ort senden. Die Kontakte findet Ihr [hier](/site/about/network_de)

#### Ihr wollt die Strukturen von Leerstandsmelder für Eure Initiative oder Euer Projekt nutzen?

Schreibt uns gerne an <hallo(at)leerstandsmelder.de>

#### Ihr möchtet einen neuen Leerstandsmelder in Eurer Region gründen? Was ist zu tun?

Findet Verbündete für eine neue Gruppe! Viele Initiativen, die sich mit Mietenwahnsinn, Klimagerechtigkeit, Stadtpolitik, Stadtentwicklung oder Regionalentwicklung beschäftigen, interessieren sich auch für das Thema Leerstand und könnten mit Euch losstarten.  
Kontaktiert uns gern, wenn sich eine Gruppe gefunden hat <neue-stadt(at)leerstandsmelder.de>

#### Gibt es Veranstaltungen zum Thema Leerstand und Stadtentwicklung in Eurer Region?

Da der Mangel an bezahlbarem Wohnraum und das Aussterben des Einzelhandels bundesweit an Brisanz zunimmt, gibt es inzwischen vielerorts Engagement und Informationsmöglichkeiten, zum Beispiel bei lokalen Initiativen, in der Presse oder in öffentlichen Programmen der Kommune.  
Bei Fragen zu aktuellen Veranstaltungen und stadtpolitischem Engagement wendet Euch vertrauensvoll an die [lokalen Leerstandsmelder](/site/about/network_de). Auch im Blog Link → Aktuell (Blog) werden Veranstaltungen oder Aktionen angekündigt; gerne beteiligt Ihr Euch und sendet uns die Ankündigung Eurer Veranstaltung an <hilfe(at)leerstandsmelder.de>

### Leerstandsmeldungen bei der Behörde

#### Lässt sich auch über behördliche Stellen erfahren, ob ein Leerstand bereits gemeldet wurde?

Bis heute (Stand April 2023) sind Informationen und Übersichten dazu nicht öffentlich zugänglich. Vereinzelt findet man Information dazu in lokalen Presseartikeln oder in den Antworten auf kleine Anfragen.

#### Ihr habt einen Leerstand bei der entsprechenden Behörde gemeldet – solltet Ihr den Leerstand trotzdem bei Leerstandsmelder eintragen?

Unbedingt! Wir erhalten (noch) keinerlei Daten von offizieller Stelle. Gerne können auch Entwicklungen, Veränderungen und aktuelle Fotos eingestellt werden.

#### Sollte Leerstand immer auch bei der zuständigen Behörde gemeldet werden?

Es ist sinnvoll, den Leerstand sowohl bei Leerstandsmelder als auch den Behörden zu melden. Dabei sind die folgenden Informationen zu beachten, siehe Link [Wie gehen Behörden mit der Leerstandsmeldung um?]  
Die behördliche Erfassung von Leerständen hilft, leer stehende Gebäude, Wohnungen, Räume oder Brachflächen einer sinnvollen Nutzung zuzuführen. Grundsätzlich ist es sinnvoll, den Behörden die Dringlichkeit des Leerstands zu signalisieren.
Die Behörden sind auf Hinweise von Bürger:innen angewiesen, um das Zweckentfremdungsverbot durchsetzen zu können.

#### Bei welcher Behörde werden Leerstände gemeldet?

Die föderale Struktur Deutschlands spiegelt sich auch in den unterschiedlichen Bezeichnungen der zuständigen Behörden wider; das kann das Amt für Baurecht und Denkmalpflege, das örtliche Bezirksamt, das Amt für Wohnungswesen, etc. sein.
In einigen Regionen kann Leerstand bei den örtlich zuständigen Behörden über ein E-Mail-Formular gemeldet werden - da keine kategorisierte Abfrage oder strukturierte Erfassung erfolgt, sind die Informationen quantitativ nicht auswertbar.

#### Wie gehen Behörden mit der Leerstandsmeldung um?

Leider gibt es von offizieller Stelle keine öffentlichen Informationen darüber, wie mit den gemeldeten Inhalten konkret verfahren wird. Die gesammelten Daten sind auch nicht öffentlich einsehbar. Die Ämter selbst verfolgen die Fälle im Rahmen ihrer Kapazitäten, sind häufig personell unterbesetzt und verfügen oft nur über eine unzureichende EDV-Ausstattung.
Auch bundesweite Kleine und Schriftliche Anfragen oder Anfragen über _[Frag den Staat ](https://fragdenstaat.de/)_ haben bisher kaum verwertbare Informationen erbracht. Oft heißt es, dass „keine Daten vorliegen“, obwohl diese theoretisch vorhanden sein müssten.

Der Umgang der Behörden mit [personenbezogenen Daten] Link (Sind personenbezogene Daten bei Leerstandsmelder sicher? ) bei Leerstandsmeldungen ist regional unterschiedlich. In Berlin beispielsweise gibt es keine gesetzliche Regelung zur Anonymisierung der persönlichen Daten der Meldenden. Einige Bezirksämter schwärzen diese Daten aus Verantwortungsbewusstsein „freiwillig“ gegenüber Dritten.  
Datenschutzverstöße sind hier aufgrund fehlender gesetzlicher Vorgaben zum Umgang mit Leerstandsmeldungen kein Einzelfall.
Die Umsetzung der Datenschutz-Grundverordnung ([DSGVO](https://dsgvo-gesetz.de/)) ist nur teilweise gegeben, da das Gesetz eine Abwägung zwischen den „schutzwürdigen Interessen der Betroffenen“ und dem „Informationsinteresse des Einsichtnehmenden“ vorsieht.
Grundsätzlich dürfen Behörden personenbezogene Daten nur speichern, verarbeiten oder übermitteln, wenn dies zur Erfüllung ihrer Aufgaben erforderlich ist.  
Meldende Mieter:innen können sich Probleme einhandeln, wenn die Eigentümer:innen infolge der Meldung ihre Namen erfahren. Um keine Risiken einzugehen, ist es ratsam, keine Leerstandsmeldung für Wohnungen zu machen, die in direktem Zusammenhang mit der eigenen Wohnung stehen, z.B. im selben Mietshaus. Das können aber leicht Bekannte oder andere Menschen aus der Nachbarschaft erledigen, deren (Miet-)Wohnungen andere Eigentümer:innen haben.

Wie sicher sind die Daten von Bürger:innen in der Behörde? Ausführlicher Artikel von Investigativ-Journalistin Gabriela Keller in der Berliner Zeitung [hier](https://www.berliner-zeitung.de/mensch-metropole/falschbehauptungen-und-datenlecks-im-bezirk-mitte-li.95617)

#### Arbeitet Leerstandsmelder mit den zuständigen Behörden für Leerstand zusammen?

Diese Frage ist regional unterschiedlich zu beantworten. Teilweise bestehen bereits Kooperationen, vielerorts wünschen wir uns aber eine direkte Nutzung und eine Zusammenarbeit mit den zuständigen Ämtern, um Bewegung in das aktive Leerstandsmanagement und die Leerstandsbeseitigung zu bringen.
Dazu wäre ein aktiver Datenaustausch (natürlich nur mit anonymisierten Nutzer:innendaten) mit den Ämtern sinnvoll. Wir arbeiten daran, dass das durch Leerstandsmelder gebündelte Engagement als Zuarbeit genutzt werden kann, um die Überprüfung der gesetzlich geregelten Fristen zu unterstützen.  
Die Plattform fordert, dass rechtliche Konsequenzen wie Bußgelder, Nachverfolgung oder Treuhänderschaft bei Zweckentfremdung umgesetzt werden. Darüber hinaus möchte Leerstandsmelder kreative Lösungen im Umgang mit _Problemimmobilien_ fördern.
