// Pinia Store
import { defineStore, acceptHMRUpdate } from "pinia";
import { v4 as uuidv4 } from "uuid";
import { useUserStore } from "@/stores/user";
import { useAuthStore } from "@/stores/auth";

import { RepositoryFactory } from "@/utils/repositories/repositoryFactory";

const baseDomain = import.meta.env.VITE_API_URL;
const baseURL = `${baseDomain}/api/v1`;

import router from "@/router";

export const usePlaceStore = defineStore({
  id: "place",
  state: () => ({
    status: "initial",
    place: {
      id: uuidv4(),
      published: false,
      title: "",
      teaser: "",
      lat: "",
      lon: "",
      city: "",
      postcode: "",
      zip: "",
      borough: "",
      suburb: "",
      road: "",
      house_number: "",
      country: "",
      country_code: "",
      user: {
        id: "",
        nickname: "",
        email: "",
      },
      buildingType: [],
      building_type: [],
      owner: [],
      status: "",
    },

    images: [],
    comments: [],
    versions: [],
  }),
  getters: {
    comment: (state) => state.comments[0],
    placeId: (state) => state.place.id,
    isEditable: (state) => {
      let userStore = useUserStore();
      let authStore = useAuthStore();
      // TODO: make editable fo rmoderators
      return state.place.user.id == userStore.user.id || authStore.isAdmin;
    },
  },
  actions: {
    fetchPlace(placeId) {
      const PlaceRepository = RepositoryFactory.get("place");
      return PlaceRepository.get(placeId).then((data) => {
        this.setPlace(data);
        this.fetchImages();
        return data;
      });
    },
    fetchPlaceAdmin(placeId) {
      const PlaceRepository = RepositoryFactory.get("place");
      let params = { versions: true };
      return PlaceRepository.get(placeId, params).then((data) => {
        this.setPlace(data);
        if (data.versions) {
          this.versions = data.versions;
        }
        return data;
      });
    },
    // load place in background
    setReloadPlace(data) {
      // raw fetch without api loader mechanism
      fetch(baseURL + `/places/${data.place.id}`)
        .then((response) => response.json())
        .then((dataJson) => {
          this.setPlace(dataJson);
        });
      return data.place;
    },

    setPlace(data) {
      // console.log("setPlacer", data);
      this.status = "loaded";
      this.place = data;
      this.images = data.images;
      this.comments = data.comments;
      return data.place;
    },

    selectPlace(data) {
      let params = {
        slug: data.slug,
      };
      if (data.region) {
        params.region = data.region;
      }
      router
        .push({
          name: "region-place",
          params: params,
        })
        .then((result) => {
          console.log("router result", result);
          //return false;
        })
        .catch((err) => {
          console.log("router catch", err);
        });
    },
    setPlaceLocationFromLookup(data) {
      let place = data.place;
      this.place.lon = place.lon;
      this.place.lat = place.lat;
      this.place.city = place.city;
      this.place.suburb = place.suburb;
      this.place.zip = place.postcode;
      //this.place.borough = place.address.borough;
      this.place.country = place.country;
      this.place.country_code = place.country_code;
      this.place.road = place.street;
      this.place.house_number = place.housenumber;
      console.log("setPlaceLocationFromLookup", this.place);
    },

    setPlaceFromLookup(data) {
      let place = data.place;
      let status = this.place.status;
      this.clear();
      this.place.status = status;
      if (place.address) {
        this.place.city = place.address.city;
        this.place.suburb = place.address.suburb;
        this.place.zip = place.address.postcode;
        this.place.borough = place.address.borough;
        this.place.country = place.address.country;
        this.place.country_code = place.address.country_code;
        this.place.road = place.address.road;
        this.place.house_number = place.address.house_number;
        if (this.place.title == "") {
          this.place.title = place.display_name;
        }
      } else {
        this.place.lon = place.lon;
        this.place.lat = place.lat;
        this.place.city = place.city;
        this.place.suburb = place.suburb;
        this.place.zip = place.postcode;
        //this.place.borough = place.address.borough;
        this.place.country = place.country;
        this.place.country_code = place.country_code;
        this.place.road = place.street;
        this.place.house_number = place.housenumber;
        if (
          !this.place.id ||
          this.place.id == "" ||
          this.place.title == "" ||
          this.status != "loaded"
        ) {
          // only set for new places
          this.place.title = place.address_line1;
        }
      }
    },
    createPlace() {
      const PlaceRepository = RepositoryFactory.get("place");
      return PlaceRepository.create(this.place).then((dataJson) => {
        this.setPlace(dataJson);
        return dataJson;
      });
    },
    deletePlace(slug) {
      const PlaceRepository = RepositoryFactory.get("place");
      return PlaceRepository.destroy(slug);
    },
    updatePlace() {
      const PlaceRepository = RepositoryFactory.get("place");
      return PlaceRepository.update(this.place.id, this.place);
    },
    updateImage(payload) {
      const ImagesRepository = RepositoryFactory.get("images");
      return ImagesRepository.update(payload);
    },
    deleteImage(slug) {
      const ImagesRepository = RepositoryFactory.get("images");
      return ImagesRepository.delete(slug);
    },
    upload(payload) {
      const { file, event } = payload;
      const params = {
        title: "",
        source: "lsm-frontend",
        place_id: this.place.id,
        file: file,
        imageable_type: "Place",
        imageable_id: this.place.id,
      };
      const ImagesRepository = RepositoryFactory.get("images");
      return ImagesRepository.upload(params, event).then((new_image) => {
        this.images.push(new_image);
        return new_image;
      });
    },
    fetchImages() {
      const ImagesRepository = RepositoryFactory.get("images");
      return ImagesRepository.get(this.place.id).then((data) => {
        this.images = data;
        return data;
      });
    },

    fetchComments() {
      const CommentsRepository = RepositoryFactory.get("comments");
      return CommentsRepository.fetch(this.place.id).then((data) => {
        this.comments = data;
        return data;
      });
    },
    clear() {
      this.$reset();
    },
  },
  persist: false,
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(usePlaceStore, import.meta.hot));
}
