import CtoCreateButton from "./CtoCreateButton.vue";

describe("CtoCreateButton", () => {
  it("should mount with label", () => {
    cy.mount(CtoCreateButton);
    cy.get("span.v-btn__content").contains("Leerstand melden");
  });

  it("when button is clicked, should call onClick", () => {
    cy.mount(CtoCreateButton, {
      props: {
        onClick: cy.spy().as("onClick"),
      },
    });

    cy.get("a.v-btn").contains("Leerstand melden").click();
    cy.get("@onClick").should("have.been.called");
  });
});
