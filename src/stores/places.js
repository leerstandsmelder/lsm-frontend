// Pinia Store
import { defineStore, acceptHMRUpdate } from "pinia";
import { RepositoryFactory } from "@/utils/repositories/repositoryFactory";

export const usePlacesStore = defineStore({
  id: "places",
  state: () => ({
    places: new Map(),
  }),
  getters: {
    getPlaces: (state) => state.places,
  },
  actions: {
    async fetchPlaces(keys) {
      const PlaceRepository = RepositoryFactory.get("place");
      let returnList = [];
      for (let key of keys) {
        if (this.places.has(key)) {
          returnList.push(this.places.get(key));
        } else {
          let element = await PlaceRepository.get(key);
          this.places.set(key, element);
          returnList.push(element);
        }
      }
      return returnList;
    },
    clear() {
      this.$reset();
    },
  },
  persist: false,
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(usePlacesStore, import.meta.hot));
}
