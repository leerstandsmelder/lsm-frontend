// Import commands.js using ES2015 syntax:
import "./commands";

import vuetify from "@/plugins/vuetify";
import { createPinia } from "pinia";
import i18n from "@/i18n";

import { mount } from "cypress/vue";

Cypress.Commands.add("mount", (component, args = {}) => {
  const root = document.getElementById("__cy_root");
  // Vuetify styling
  if (!root.classList.contains("v-application")) {
    root.classList.add("v-application");
  }
  // Vuetify selector used for popup elements to attach to the DOM
  root.setAttribute("data-app", "true");

  i18n.global.locale.value = 'de';
  i18n.locale = 'de';

  args.global = args.global || {};
  args.global.plugins = args.global.plugins || [];
  args.global.plugins.push(createPinia());
  args.global.plugins.push(vuetify);
  args.global.plugins.push(i18n);

  return mount(component, {
    ...args, // To override values for specific tests
  });
});
