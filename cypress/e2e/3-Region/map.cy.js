/// <reference types="cypress" />

describe("Region screen", () => {
  beforeEach(() => {
    cy.intercept_all_apis();

    cy.log('Cypress.env("baseUrl")....', Cypress.env("baseUrl"));
    cy.visit("/region");
  });

  it("displays layout elements", () => {
    cy.get(".region-map").should("have.length", 1);
    cy.get(".maplibregl-map").should("have.length", 1);
    cy.get("canvas.maplibregl-canvas").should("have.length", 1);
  });

  it("loads and displays the region map", () => {
    cy.viewport("iphone-8");

    cy.get(".maplibregl-map").should("be.visible");
    cy.get(".maplibregl-map").invoke("height").should("be.eq", 600);
  });
});
